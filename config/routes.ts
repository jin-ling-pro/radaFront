﻿import FileSetting from "@/pages/Data/FileSetting";

export default [
  {
    path: '/zhiyuan',
    name: '远程控制',
    icon: 'table',
    routes: [
      {
        path: '/zhiyuan/controllPage',
        name: '控制记录',
        component: './Chart/PythonChart/ZhiYuan/ControllHistory.tsx',
      },
      {
        path: '/zhiyuan/VAD',
        name: 'VAD/DBS',
        component: './Chart/PythonChart/Plus/VAD',
      },
      {
        path: '/zhiyuan/PPI',
        name: 'PPI',
        component: './Chart/PythonChart/Plus/PPI',
      },
      {
        path: '/zhiyuan/RHI',
        name: 'RHI',
        component: './Chart/PythonChart/Plus/RHI',
      },
    ],
  },
  // {
  //   path: '/',
  //   name: '远程监控',
  //   icon: 'table',
  //   component: './Chart/PythonChart/index2',
  // },
  // {
  //   path: '/control',
  //   name: '雷达控制页面',
  //   icon: 'table',
  //   routes: [
  //     {
  //       path: '/control/page1',
  //       name: '雷达控制页面1',
  //       component: './ControlPages/LidarPage1.tsx',
  //     },
  //     {
  //       path: '/control/page2',
  //       name: '雷达控制页面2',
  //       component: './ControlPages/LidarPage2.tsx',
  //     },
  //   ],
  // },
  {
    path: '/',
    name: '大屏',
    icon: 'table',
    component: './Index/index',
  },
  {
    path: '/RadarHistory',
    name: '历史数据',
    icon: 'table',
    component: './Data/RadarHistory',
  },
  // {
  //   path: '/AccessManagement',
  //   name: '权限管理',
  //   icon: 'table',
  //   component: './User/UserManagement/AccessManagement',
  // },
  //-------------------模板-----------------------------------------
  {
    path: '/status',
    name: '状态监控',
    icon: 'table',
    routes: [
      {
        name: '激光器状态',
        path: '/status/laser',
        component: './Status/LaserPage',
      },
      {
        name: '环境状态',
        path: '/status/environment',
        component: './Status/Environment',
      },
      {
        name: '倾斜角传感器状态',
        path: '/status/tilt',
        component: './Status/TiltPage',
      },
      {
        name: '温湿度传感器状态',
        path: '/status/temp',
        component: './Status/TempPage',
      },
    ],
  },
  {
    path: '/warning',
    name: '设备预警',
    icon: 'table',
    routes: [
      {
        name: '预警设置',
        path: '/warning/setting',
        component: './WarningPages/WarningSettings',
      },
      {
        name: '告警记录',
        path: '/warning/log',
        component: './WarningPages/WindShearWarning',
      },
    ],
  },
  {
    path: '/data',
    name: '数据处理',
    icon: 'table',
    routes: [
      {
        name: '数据文件',
        path: '/data/file',
        component: './Data/RadarHistory',
      },
      {
        name: '文件配置',
        path: '/data/fileSetting',
        component: './Data/FileSetting',
      },
      // {
      //   name: '预警设置',
      //   path: '/data/login',
      //   component: './Data/WarningPage/WarningSetting',
      // },
    ],
  },
  {
    path: '/maintenance',
    name: '维护维修',
    icon: 'table',
    routes: [
      {
        name: '维护维修',
        path: '/maintenance/page',
        component: './Maintenance/MaintenancePage',
      },
      {
        name: '新增记录',
        path: '/maintenance/log',
        component: './Maintenance/MaintenanceLog',
      },
    ],
  },
  {
    path: '/parameter',
    name: '参数设置',
    icon: 'table',
    routes: [
      {
        name: '站点信息管理',
        path: '/parameter/site',
        component: './para/SiteInfo',
      },
      {
        name: '雷达信息管理',
        path: '/parameter/radar',
        component: './para/RadarInfo',
      },
      {
        name: '工控机参数设置',
        path: '/parameter/ipc',
        component: './para/ipc',
      },
    ],
  },
  {
    path: '/calibration',
    name: '标定管理',
    icon: 'table',
    routes: [
      {
        name: '指北标定',
        path: '/calibration/1',
        component: './Calibration/CalibrationPage',
      },
      {
        name: '标定记录',
        path: '/calibration/2',
        component: './Calibration/CalibrationLog',
      },
    ],
  },
  {
    path: '/software',
    name: '软件管理',
    icon: 'table',
    routes: [
      {
        name: '软件管理',
        path: '/software/softwareSetting',
        component: './Software/softwareSetting',
      },
      {
        name: '新增',
        path: '/software/log',
        component: './Software/Log',
      },
    ],
  },
  // {
  //   path: '/hardware',
  //   name: '设备管理',
  //   icon: 'table',
  //   component: './User/Login',
  // },


  {
    path: '/user',
    layout: false,
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './User/Login',
      },
    ],
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    routes: [
      {
        path: '/admin',
        redirect: '/admin/sub-page',
      },
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        component: './Admin',
      },
    ],
  },
  {
    path: '*',
    layout: false,
    component: './404',
  },
];
