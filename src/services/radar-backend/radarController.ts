// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** getStatus GET /api/radar/getStatus */
export async function getStatusUsingGet(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseWindLidarData_>('/api/radar/getStatus', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** getStatus PUT /api/radar/getStatus */
export async function getStatusUsingPut(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseWindLidarData_>('/api/radar/getStatus', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** getStatus POST /api/radar/getStatus */
export async function getStatusUsingPost(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseWindLidarData_>('/api/radar/getStatus', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** getStatus DELETE /api/radar/getStatus */
export async function getStatusUsingDelete(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseWindLidarData_>('/api/radar/getStatus', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** getStatus PATCH /api/radar/getStatus */
export async function getStatusUsingPatch(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseWindLidarData_>('/api/radar/getStatus', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** init GET /api/radar/init */
export async function initUsingGet(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseString_>('/api/radar/init', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** init PUT /api/radar/init */
export async function initUsingPut(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseString_>('/api/radar/init', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** init POST /api/radar/init */
export async function initUsingPost(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseString_>('/api/radar/init', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** init DELETE /api/radar/init */
export async function initUsingDelete(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseString_>('/api/radar/init', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** init PATCH /api/radar/init */
export async function initUsingPatch(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseString_>('/api/radar/init', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** start GET /api/radar/start */
export async function startUsingGet(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBoolean_>('/api/radar/start', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** start PUT /api/radar/start */
export async function startUsingPut(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBoolean_>('/api/radar/start', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** start POST /api/radar/start */
export async function startUsingPost(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBoolean_>('/api/radar/start', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** start DELETE /api/radar/start */
export async function startUsingDelete(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBoolean_>('/api/radar/start', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** start PATCH /api/radar/start */
export async function startUsingPatch(
  body: API.RadarCommandRequest,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBoolean_>('/api/radar/start', {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
