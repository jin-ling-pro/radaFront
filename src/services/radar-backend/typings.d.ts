declare namespace API {
  type CommandData = {
    azimuthSpeed?: number;
    azimuthAngle?: number;
    elevationSpeed?: number;
    elevationAngle?: number;
    startAngle?: number;
    endAngle?: number;
    scanType?: string;
    quantity?: number;
    angleSync?: string;
    remarks?: string;
    elevationMode?: string;
    cycleNum?: number; // 圈数
    detectionMode: string; // VAD 或 DBS
    // 添加其他需要的属性
  };

  type VADRequest = {
    commandType: string; // 例如 "VAD" 或 "DBS"
    commandData: CommandData; // commandData 包含复杂结构
    signal?: string;
  };

  type BaseResponseBoolean_ = {
    code?: number;
    data?: boolean;
    description?: string;
    message?: string;
  };

  type BaseResponseInt_ = {
    code?: number;
    data?: number;
    description?: string;
    message?: string;
  };

  type BaseResponseListUser_ = {
    code?: number;
    data?: User[];
    description?: string;
    message?: string;
  };

  type BaseResponseLong_ = {
    code?: number;
    data?: number;
    description?: string;
    message?: string;
  };

  type BaseResponseString_ = {
    code?: number;
    data?: string;
    description?: string;
    message?: string;
  };

  type BaseResponseUser_ = {
    code?: number;
    data?: User;
    description?: string;
    message?: string;
  };

  type BaseResponseWindLidarData_ = {
    code?: number;
    data?: WindLidarData;
    description?: string;
    message?: string;
  };

  type RadarCommandRequest = {
    commandData?: string | null;
    commandType?: string;
    id?: number;
    radarStatus?: number;
  };

  type searchUserUsingGETParams = {
    /** username */
    username?: string;
  };

  type User = {
    avatarUrl?: string;
    createTime?: string;
    department?: string;
    email?: string;
    gender?: number;
    id?: number;
    isDelete?: number;
    phone?: string;
    updateTime?: string;
    userAccount?: string;
    userPassword?: string;
    userRole?: number;
    userStatus?: number;
    username?: string;
  };

  type UserLoginRequest = {
    checkPassword?: string;
    userAccount?: string;
    userPassword?: string;
  };

  type UserRegisterRequest = {
    checkPassword?: string;
    userAccount?: string;
    userPassword?: string;
  };

  type WindLidarData = true;
}
