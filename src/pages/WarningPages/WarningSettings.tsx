import React, { useState } from 'react';
import { Form, Input, Select, Button, Table, Space, Typography } from 'antd';

const { Option } = Select;
const { TextArea } = Input;

const WarningSettings = () => {
  const [form] = Form.useForm();
  const [warnings, setWarnings] = useState([]);

  const handleAdd = () => {
    form.validateFields().then(values => {
      setWarnings([...warnings, { ...values, key: warnings.length }]);
      form.resetFields(['field', 'rule', 'maxValue', 'minValue']);
    });
  };

  const handleDelete = (key) => {
    setWarnings(warnings.filter((item) => item.key !== key));
  };

  const columns = [
    { title: '预警字段', dataIndex: 'field', key: 'field' },
    { title: '预警规则', dataIndex: 'rule', key: 'rule' },
    { title: '预警值(最大)', dataIndex: 'maxValue', key: 'maxValue' },
    { title: '最小预警值', dataIndex: 'minValue', key: 'minValue' },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Button type="link" onClick={() => handleDelete(record.key)} danger>
          删除
        </Button>
      ),
    },
  ];

  return (
    <div style={{ padding: 24, backgroundColor: '#fff', borderRadius: 8 }}>
      <Typography.Title level={3} style={{ color: '#1890ff' }}>
        监控预警设置
      </Typography.Title>
      <Form form={form} layout="inline" style={{ marginBottom: 16 }}>
        <Form.Item name="type" label="预警类型" rules={[{ required: true, message: '请输入内容' }]}>
          <Input placeholder="请输入内容" />
        </Form.Item>
        <Form.Item name="name" label="预警名称" rules={[{ required: true, message: '请输入内容' }]}>
          <Input placeholder="请输入内容" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" onClick={handleAdd}>添加</Button>
        </Form.Item>
      </Form>

      <Form form={form} layout="inline" style={{ marginBottom: 16 }}>
        <Form.Item name="field" label="预警字段" rules={[{ required: true, message: '请选择字段' }]}>
          <Select placeholder="请选择">
            <Option value="水平风速">水平风速</Option>
            <Option value="垂直风速">垂直风速</Option>
          </Select>
        </Form.Item>
        <Form.Item name="rule" label="预警规则" rules={[{ required: true, message: '请选择规则' }]}>
          <Select placeholder="请选择">
            <Option value="大于">大于</Option>
            <Option value="小于">小于</Option>
          </Select>
        </Form.Item>
        <Form.Item name="maxValue" label="预警值(最大)" rules={[{ required: true, message: '请输入数值' }]}>
          <Input placeholder="请输入内容" />
        </Form.Item>
        <Form.Item name="minValue" label="最小预警值" rules={[{ required: true, message: '请输入数值' }]}>
          <Input placeholder="请输入内容" />
        </Form.Item>
      </Form>

      <Table columns={columns} dataSource={warnings} pagination={false} style={{ marginBottom: 16 }} />

      <Form.Item label="预警提示" style={{ width: '100%' }}>
        <TextArea rows={3} placeholder="请输入内容" />
      </Form.Item>

      <Space style={{ marginTop: 16 }}>
        <Button type="primary">确定</Button>
        <Button>取消</Button>
      </Space>
    </div>
  );
};

export default WarningSettings;
