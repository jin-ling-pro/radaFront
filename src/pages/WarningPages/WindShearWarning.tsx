import React, { useState, useEffect } from 'react';
import { Table, Form, Input, Button, DatePicker, Select, Pagination, Badge, Tag } from 'antd';
import { SearchOutlined, ReloadOutlined } from '@ant-design/icons';
import moment from 'moment';

const { RangePicker } = DatePicker;
const { Option } = Select;

const WindShearWarning = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [pagination, setPagination] = useState({ current: 1, pageSize: 10, total: 0 });

  useEffect(() => {
    fetchData();
  }, [pagination.current, pagination.pageSize]);

  const fetchData = async () => {
    setLoading(true);
    // 模拟API请求数据
    setTimeout(() => {
      const fakeData = Array.from({ length: 125 }, (_, index) => ({
        key: index,
        time: moment().subtract(index, 'minutes').format('YYYY-MM-DD HH:mm:ss'),
        type: index % 2 === 0 ? '侧风切变' : '侧风超限',
        warningLevel: index % 3 === 0 ? '中度' : (index % 3 === 1 ? '轻度' : '重度'),
        windSpeed: (Math.random() * 20 - 10).toFixed(3),
        alertRange: index % 2 === 0 ? '1NM' : '2NM-3NM',
        height: 60 + index * 30,
        runway: 30
      }));
      setData(fakeData.slice((pagination.current - 1) * pagination.pageSize, pagination.current * pagination.pageSize));
      setPagination(prev => ({ ...prev, total: fakeData.length }));
      setLoading(false);
    }, 1000);
  };

  const handleSearch = () => {
    // 处理查询表单逻辑
    form.validateFields().then(values => {
      console.log('查询参数:', values);
      fetchData();
    });
  };

  const handleReset = () => {
    form.resetFields();
    fetchData();
  };

  const columns = [
    { title: '时间', dataIndex: 'time', key: 'time' },
    { title: '切变类型', dataIndex: 'type', key: 'type' },
    {
      title: '切变强度',
      dataIndex: 'warningLevel',
      key: 'warningLevel',
      render: (level) => (
        <Tag color={level === '中度' ? 'green' : level === '轻度' ? 'gold' : 'red'}>
          {level}
        </Tag>
      ),
    },
    { title: '风速变化值 (m/s)', dataIndex: 'windSpeed', key: 'windSpeed' },
    { title: '告警范围', dataIndex: 'alertRange', key: 'alertRange' },
    { title: '高度 (m)', dataIndex: 'height', key: 'height' },
    { title: '跑道编号', dataIndex: 'runway', key: 'runway' },
    {
      title: '操作',
      key: 'operation',
      render: () => <Button type="primary">查看详情</Button>,
    },
  ];

  const onTableChange = (page) => {
    setPagination(prev => ({ ...prev, current: page }));
  };

  return (
    <div>
      {/* 查询表单 */}
      <Form form={form} layout="inline" style={{ marginBottom: 20 }}>
        <Form.Item name="dateRange" label="时间范围">
          <RangePicker />
        </Form.Item>
        <Form.Item name="windType" label="切变类型">
          <Select style={{ width: 150 }} placeholder="选择切变类型">
            <Option value="1">侧风切变</Option>
            <Option value="2">侧风超限</Option>
          </Select>
        </Form.Item>
        <Form.Item name="warningLevel" label="切变强度">
          <Select style={{ width: 150 }} placeholder="选择切变强度">
            <Option value="轻度">轻度</Option>
            <Option value="中度">中度</Option>
            <Option value="重度">重度</Option>
          </Select>
        </Form.Item>
        <Form.Item>
          <Button type="primary" icon={<SearchOutlined />} onClick={handleSearch}>
            查询
          </Button>
        </Form.Item>
        <Form.Item>
          <Button icon={<ReloadOutlined />} onClick={handleReset}>
            重置
          </Button>
        </Form.Item>
      </Form>

      {/* 数据表格 */}
      <Table
        columns={columns}
        dataSource={data}
        loading={loading}
        pagination={false}
        rowKey="key"
      />

      {/* 分页控件 */}
      <Pagination
        style={{ marginTop: 20, textAlign: 'right' }}
        current={pagination.current}
        pageSize={pagination.pageSize}
        total={pagination.total}
        onChange={onTableChange}
      />
    </div>
  );
};

export default WindShearWarning;
