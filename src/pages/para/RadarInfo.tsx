import React from 'react';
import { ProForm, ProFormText, ProFormGroup, ProFormSelect } from '@ant-design/pro-components';
import { Button, Divider } from 'antd';

const RadarInfoPage = () => {
  return (
    <div style={{ padding: '20px', background: '#ffffff', borderRadius: '8px', boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)' }}>
      <h2 style={{ color: '#1a73e8' }}>雷达管理</h2>
      <Divider style={{ margin: '12px 0', borderColor: '#d9d9d9' }} />

      {/* 雷达基本信息 */}
      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '16px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>雷达基本信息</h3>

        <ProForm layout="horizontal" submitter={false}>
          <ProFormGroup>
            <ProFormText name="radarName" label="雷达名称" placeholder="没有英文字段 数字 -90.0~90.0°" rules={[{ required: true, message: '请输入雷达名称' }]} />
            <ProFormSelect name="radarType" label="雷达类型" options={[{ value: '相干脉冲', label: '相干脉冲' }, { value: '非相干脉冲', label: '非相干脉冲' }]} placeholder="选择雷达类型" rules={[{ required: true, message: '请选择雷达类型' }]} />
          </ProFormGroup>
          <ProFormGroup>
            <ProFormText name="mainVersion" label="主版本号" placeholder="Magic Number 整型 范围: 1~255" rules={[{ required: true, message: '请输入主版本号' }]} />
            <ProFormText name="subVersion" label="次版本号" placeholder="Minor Version 整型 范围: 0~255" rules={[{ required: true, message: '请输入次版本号' }]} />
          </ProFormGroup>
          <ProFormGroup>
            <ProFormText name="radarCode" label="雷达型号" placeholder="远距离 (LR)" rules={[{ required: true, message: '请输入雷达型号' }]} />
          </ProFormGroup>
        </ProForm>
      </div>

      {/* 雷达技术参数 */}
      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>雷达技术参数</h3>

        <ProForm layout="horizontal" submitter={false}>
          <ProFormGroup>
            <ProFormText name="maxRange" label="最大探测距离" placeholder="Maximum Range 整型 1~100000 米" rules={[{ required: true, message: '请输入最大探测距离' }]} />
            <ProFormText name="maxSpeed" label="最大探测速度" placeholder="Max Measurement Speed 数字 0.0~200.0" rules={[{ required: true, message: '请输入最大探测速度' }]} />
          </ProFormGroup>
          <ProFormGroup>
            <ProFormText name="startRange" label="起始距离" placeholder="Start Range 数字 1~100000 米" rules={[{ required: true, message: '请输入起始距离' }]} />
            <ProFormText name="windowHeight" label="发射窗口高度" placeholder="Window Height 数字 0.0~1000.0 米" rules={[{ required: true, message: '请输入发射窗口高度' }]} />
          </ProFormGroup>
          <ProFormGroup>
            <ProFormText name="cnrThreshold" label="载噪比阈值" placeholder="CNR Threshold 数字 -100.0~100.0 db" rules={[{ required: true, message: '请输入载噪比阈值' }]} />
            <ProFormText name="adjustAngle" label="调平角度" placeholder="没有英文字段 数字 -90.0~90.0°" rules={[{ required: true, message: '请输入调平角度' }]} />
          </ProFormGroup>
          <ProFormGroup>
            <ProFormText name="rangeResolution" label="距离分辨率" placeholder="Range Resolution 数字 1~1000 米" rules={[{ required: true, message: '请输入距离分辨率' }]} />
          </ProFormGroup>
        </ProForm>
      </div>

      {/* 确定和取消按钮 */}
      <div style={{ textAlign: 'right', marginTop: '16px' }}>
        <Button type="default" style={{ marginRight: '8px', color: '#0073e6', borderColor: '#0073e6' }}>确定</Button>
        <Button type="default">取消</Button>
      </div>
    </div>
  );
};

export default RadarInfoPage;
