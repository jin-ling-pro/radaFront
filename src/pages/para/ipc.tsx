import React from 'react';
import { ProForm, ProFormText, ProFormSelect, ProFormSwitch, ProFormGroup } from '@ant-design/pro-components';
import { Divider, Button } from 'antd';

const ControlPanelPage = () => {
  return (
    <div style={{ padding: '20px', background: '#ffffff', borderRadius: '8px', boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)' }}>
      <h2 style={{ color: '#1a73e8' }}>工控机参数设置</h2>
      <Divider style={{ margin: '12px 0', borderColor: '#d9d9d9' }} />

      {/* 激光器参数设置 */}
      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '16px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>激光器参数设置</h3>

        <ProForm layout="horizontal" submitter={false}>
          <Divider>静态参数</Divider>
          <ProFormGroup>
            <ProFormText name="comPort" label="串口号：" placeholder="请输入内容" />
            <ProFormText name="baudRate" label="波特率：" placeholder="请输入内容" />
            <ProFormText name="aomFrequencyShift" label="AOM 频移量：" placeholder="0.0~200.0 MHz" />
            <ProFormText name="laserWavelength" label="激光波长：" placeholder="1~100000 纳米" />
            <ProFormSwitch name="comPortSwitch" label="串口开关：" checkedChildren="开" unCheckedChildren="关" />
            <ProFormSwitch name="laserSwitch" label="激光器开关：" checkedChildren="开" unCheckedChildren="关" />
          </ProFormGroup>

          <Divider>动态参数</Divider>
          <ProFormGroup>
            <ProFormSelect
              name="pulseWidth"
              label="脉宽："
              options={[
                { label: '800 ns', value: '800' },
                { label: '1000 ns', value: '1000' },
              ]}
              placeholder="选择脉宽"
            />
            <ProFormText name="frequency" label="频率：" />
            <ProFormText name="power" label="功率：" placeholder="请输入内容" />
          </ProFormGroup>
        </ProForm>
      </div>

      {/* 转台参数设置 */}
      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '16px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>转台参数设置</h3>

        <ProForm layout="horizontal" submitter={false}>
          <Divider>静态参数</Divider>
          <ProFormGroup>
            <ProFormText name="comPortRotation" label="串口号：" placeholder="请输入内容" />
            <ProFormText name="baudRateRotation" label="波特率：" placeholder="请输入内容" />
            <ProFormSwitch name="rotationSwitch" label="转台串口开关：" checkedChildren="开" unCheckedChildren="关" />
          </ProFormGroup>

          <Divider>动态参数</Divider>
          <ProFormGroup>
            <ProFormSelect name="workModeAzimuth" label="方位工作模式：" options={[{ label: '模式1', value: 'mode1' }]} />
            <ProFormSelect name="workModeElevation" label="俯仰工作模式：" options={[{ label: '模式2', value: 'mode2' }]} />
            <ProFormText name="scanSpeedAzimuth" label="方位扫描速度：" placeholder="0.0~360.0 度/秒" />
            <ProFormText name="scanSpeedElevation" label="俯仰扫描速度：" placeholder="0.0~90.0 度/秒" />
            <ProFormText name="angleAzimuth" label="方位角度：" placeholder="0.0~360.0 度" />
            <ProFormText name="angleElevation" label="俯仰角度：" placeholder="-10.0~90.0 度" />
          </ProFormGroup>
        </ProForm>
      </div>

      {/* 其他参数设置（示例） */}
      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '16px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>采集卡参数设置</h3>
        <ProForm layout="horizontal" submitter={false}>
          <Divider>动态参数</Divider>
          <ProFormGroup>
            <ProFormText name="samplesPerGate" label="Samples per Gate：" placeholder="请输入内容" />
            <ProFormText name="pulsesNumber" label="Pulses Number：" placeholder="请输入内容" />
            <ProFormText name="prtLen" label="Prt_len：" placeholder="请输入内容" />
            <ProFormText name="gateNumber" label="Gate Number：" placeholder="请输入内容" />
            <ProFormText name="fftStart" label="FFT_Start：" placeholder="请输入内容" />
            <ProFormText name="fftLen" label="FFT_LEN：" placeholder="512" disabled />
          </ProFormGroup>
        </ProForm>
      </div>

      {/* 保存和取消按钮 */}
      <div style={{ textAlign: 'right', marginTop: '16px' }}>
        <Button type="default" style={{ marginRight: '8px', color: '#0073e6', borderColor: '#0073e6' }}>确定</Button>
        <Button type="default">取消</Button>
      </div>
    </div>
  );
};

export default ControlPanelPage;
