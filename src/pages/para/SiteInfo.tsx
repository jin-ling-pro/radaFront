import React from 'react';
import { ProForm, ProFormText, ProFormGroup } from '@ant-design/pro-components';
import { Button, Divider } from 'antd';

const SiteInfoPage = () => {
  return (
    <div style={{ padding: '20px', background: '#ffffff', borderRadius: '8px', boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)' }}>
      <h2 style={{ color: '#1a73e8' }}>站点管理</h2>
      <Divider style={{ margin: '12px 0', borderColor: '#d9d9d9' }} />

      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '16px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>站点基本信息</h3>

        <ProForm
          layout="horizontal"
          submitter={{
            render: () => (
              <>
                <Button type="default" style={{ marginRight: '8px', color: '#0073e6', borderColor: '#0073e6' }}>确定</Button>
                <Button type="default">取消</Button>
              </>
            ),
          }}
        >
          <ProFormGroup>
            <ProFormText name="siteName" label="站点名称" placeholder="Site Name 字符串" rules={[{ required: true, message: '请输入站点名称' }]} />
            <ProFormText name="siteCode" label="站号" placeholder="Site Code 字符串" rules={[{ required: true, message: '请输入站号' }]} />
          </ProFormGroup>

          <ProFormGroup>
            <ProFormText name="longitude" label="经度" placeholder="Longitude 数字 范围：-180.0°~180.0°" rules={[{ required: true, message: '请输入经度' }]} />
            <ProFormText name="latitude" label="纬度" placeholder="Latitude 数字 范围：-90.0°~90.0°" rules={[{ required: true, message: '请输入纬度' }]} />
          </ProFormGroup>

          <ProFormGroup>
            <ProFormText name="siteHeight" label="站点高度" placeholder="Site Height 数字 范围：-160.0~9000.0 米" rules={[{ required: true, message: '请输入站点高度' }]} />
          </ProFormGroup>
        </ProForm>
      </div>
    </div>
  );
};

export default SiteInfoPage;
