import React from 'react';
import { ProForm, ProFormText, ProFormRadio, ProFormGroup } from '@ant-design/pro-components';
import {Button, Divider} from 'antd';

const ConfigSection = ({ title, config }) => (
  <div style={{ padding: '20px', background: '#ffffff', borderRadius: '8px', boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)', marginBottom: '20px' }}>
    <h2 style={{ color: '#1a73e8' }}>{title}</h2>
    <Divider style={{ margin: '12px 0', borderColor: '#d9d9d9' }} />

    <ProForm layout="horizontal" submitter={false}>
      <ProFormGroup>
        <ProFormText name="field1" label={config.field1Label} placeholder={config.field1Placeholder} />
        <ProFormText name="field2" label={config.field2Label} placeholder={config.field2Placeholder} />
      </ProFormGroup>

      <ProFormGroup>
        <ProFormText name="field3" label={config.field3Label} placeholder={config.field3Placeholder} />
        <ProFormText name="field4" label={config.field4Label} placeholder={config.field4Placeholder} />
      </ProFormGroup>

      {config.radioOptions && (
        <ProFormGroup>
          <ProFormRadio.Group name="scanType" label="扫描方式" options={config.radioOptions} />
        </ProFormGroup>
      )}

      {config.extraFields && (
        <ProFormGroup>
          {config.extraFields.map((field, index) => (
            <ProFormText key={index} name={field.name} label={field.label} placeholder={field.placeholder} disabled />
          ))}
        </ProFormGroup>
      )}
    </ProForm>

    {/* 添加操作按钮 */}
    <div style={{ marginTop: '16px' }}>
      <Button type="default" style={{ marginRight: '8px', color: '#0073e6', borderColor: '#0073e6' }}>设置</Button>
      <Button type="primary" style={{ backgroundColor: '#0073e6', borderColor: '#0073e6' }}>确定</Button>
      {/*<Button type="default" style={{ marginLeft: '8px', color: '#0073e6', borderColor: '#0073e6' }}>复位</Button>*/}
    </div>
  </div>
);

const FileSetting = () => {
  const receiveConfig = {
    field1Label: 'IP地址',
    field1Placeholder: '请输入IP地址',
    field2Label: 'API地址',
    field2Placeholder: '请输入API地址',
    field3Label: 'API类型',
    field3Placeholder: '请输入API类型',
    field4Label: 'API参数',
    field4Placeholder: '请输入API参数',
    radioOptions: [
      { label: '持续扫', value: '持续扫' },
      { label: '扇扫', value: '扇扫' },
    ],
    extraFields: [
      { name: 'quantity', label: '方位同步数', placeholder: '数据固定显示' },
      { name: 'angleSync', label: '实时方位角', placeholder: '数据固定显示' },
      { name: 'remarks', label: '备注', placeholder: '数据固定显示' },
    ],
  };

  const storageConfig = {
    field1Label: '内存分配',
    field1Placeholder: '请输入内存分配',
    field2Label: '存储时间',
    field2Placeholder: '请输入存储时间',
    field3Label: '备份频率',
    field3Placeholder: '选择备份频率',
    field4Label: '备份类型',
    field4Placeholder: '选择备份类型',
  };

  const uploadConfig = {
    field1Label: '目标IP',
    field1Placeholder: '请输入目标IP',
    field2Label: '上传文件类型',
    field2Placeholder: '选择上传文件类型',
    field3Label: '用户名',
    field3Placeholder: '请输入用户名',
    field4Label: '密码',
    field4Placeholder: '请输入密码',
  };

  return (
    <div style={{ padding: '24px', background: '#f5faff' }}>
      <h1 style={{ marginBottom: '24px', color: '#1a73e8' }}>文件配置</h1>
      <ConfigSection title="数据接收设置" config={receiveConfig} />
      <ConfigSection title="数据存储设置" config={storageConfig} />
      <ConfigSection title="数据上传设置" config={uploadConfig} />
    </div>
  );
};

export default FileSetting;
