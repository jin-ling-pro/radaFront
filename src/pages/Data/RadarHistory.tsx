import React, { useState, useEffect } from 'react';
import { Table, Form, Input, Button, DatePicker, Select, Pagination } from 'antd';
import { SearchOutlined, ReloadOutlined } from '@ant-design/icons';
import moment from 'moment';

const { RangePicker } = DatePicker;
const { Option } = Select;

const RadarHistory = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [pagination, setPagination] = useState({ current: 1, pageSize: 10, total: 0 });

  useEffect(() => {
    fetchData();
  }, [pagination.current, pagination.pageSize]);

  const fetchData = async () => {
    setLoading(true);
    // 模拟API请求
    setTimeout(() => {
      const fakeData = Array.from({ length: 50 }, (_, index) => ({
        key: index,
        radarNumber: `R${index}`,
        fileName: `RadarFile_${index}.bin`,
        scanType: '扫描模式1',
        date: moment().subtract(index, 'days').format('YYYY-MM-DD'),
        fileSize: `${(Math.random() * 100).toFixed(2)} MB`,
      }));
      setData(fakeData.slice((pagination.current - 1) * pagination.pageSize, pagination.current * pagination.pageSize));
      setPagination(prev => ({ ...prev, total: fakeData.length }));
      setLoading(false);
    }, 1000);
  };

  const handleSearch = () => {
    // 获取表单的值进行搜索
    form.validateFields().then(values => {
      console.log(values);
      fetchData();
    });
  };

  const handleReset = () => {
    form.resetFields();
    fetchData();
  };

  const columns = [
    { title: '雷达编号', dataIndex: 'radarNumber', key: 'radarNumber' },
    { title: '文件名称', dataIndex: 'fileName', key: 'fileName' },
    { title: '扫描模式', dataIndex: 'scanType', key: 'scanType' },
    { title: '数据日期', dataIndex: 'date', key: 'date' },
    { title: '文件大小', dataIndex: 'fileSize', key: 'fileSize' },
    { title: '操作', key: 'operation', render: () => <Button>下载</Button> },
  ];

  const onTableChange = (page) => {
    setPagination(prev => ({ ...prev, current: page }));
  };

  return (
    <div>
      {/* 查询表单 */}
      <Form form={form} layout="inline" style={{ marginBottom: 20 }}>
        <Form.Item name="radarNumber" label="雷达编号">
          <Input placeholder="请输入雷达编号" />
        </Form.Item>
        <Form.Item name="scanType" label="扫描模式">
          <Select style={{ width: 150 }} placeholder="选择扫描模式">
            <Option value="1">模式1</Option>
            <Option value="2">模式2</Option>
          </Select>
        </Form.Item>
        <Form.Item name="dateRange" label="开始日期-结束日期">
          <RangePicker />
        </Form.Item>
        <Form.Item>
          <Button type="primary" icon={<SearchOutlined />} onClick={handleSearch}>
            查询
          </Button>
        </Form.Item>
        <Form.Item>
          <Button icon={<ReloadOutlined />} onClick={handleReset}>
            重置
          </Button>
        </Form.Item>
      </Form>

      {/* 数据表格 */}
      <Table
        columns={columns}
        dataSource={data}
        loading={loading}
        pagination={false}
        rowKey="key"
      />

      {/* 分页控件 */}
      <Pagination
        style={{ marginTop: 20, textAlign: 'right' }}
        current={pagination.current}
        pageSize={pagination.pageSize}
        total={pagination.total}
        onChange={onTableChange}
      />
    </div>
  );
};

export default RadarHistory;
