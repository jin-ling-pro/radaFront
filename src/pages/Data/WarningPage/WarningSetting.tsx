import React from 'react';
import { Row, Col } from 'antd';
import WarningCards from './WarningCards';
import WarningThresholdTable from './WarningRight';

const WarningDashboardPage = () => {
  return (
    <div style={{ padding: '24px', backgroundColor: '#f0f2f5' }}>
      <Row gutter={16}>
        <Col span={6}>
          <WarningCards />
        </Col>
        <Col span={18}>
          <WarningThresholdTable />
        </Col>
      </Row>
    </div>
  );
};

export default WarningDashboardPage;
