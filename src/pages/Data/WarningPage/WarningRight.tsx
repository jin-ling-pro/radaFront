import React from 'react';
import { Table, Button } from 'antd';

const WarningThresholdTable = () => {
  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      key: 'index',
    },
    {
      title: '预警名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '预警类型',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: '预警字段',
      dataIndex: 'field',
      key: 'field',
    },
    {
      title: '阈值规则',
      dataIndex: 'rule',
      key: 'rule',
    },
    {
      title: '预警规则',
      dataIndex: 'threshold',
      key: 'threshold',
    },
    {
      title: '预警提示',
      dataIndex: 'tip',
      key: 'tip',
    },
    {
      title: '操作',
      key: 'action',
      render: () => (
        <>
          <Button type="link">编辑</Button>
          <Button type="link" danger>
            删除
          </Button>
        </>
      ),
    },
  ];

  const data = [
    {
      key: '1',
      index: '1',
      name: 'PPI扫描水平风速预警',
      type: 'PPI',
      field: '水平风速',
      rule: '大于',
      threshold: 'X > 15.2',
      tip: '当前风速超过X级，请注意抗灾抢险',
    },
    {
      key: '2',
      index: '2',
      name: 'PPI扫描垂直风速预警',
      type: 'PPI',
      field: '垂直风速',
      rule: '范围',
      threshold: '20.1 > X > 13.5',
      tip: '当前风速超过X级，请注意抗灾抢险',
    },
  ];

  return (
    <div style={{ padding: '24px', backgroundColor: '#ffffff', borderRadius: '8px' }}>
      <h2 style={{ color: '#1890ff', padding: '10px 0', borderBottom: '2px solid #1890ff' }}>预警阈值列表</h2>
      <Table
        columns={columns}
        dataSource={data}
        pagination={{ pageSize: 10, showSizeChanger: true }}
        bordered
        rowKey="key"
      />
    </div>
  );
};

export default WarningThresholdTable;
