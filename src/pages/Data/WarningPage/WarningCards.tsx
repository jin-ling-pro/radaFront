import React from 'react';
import { Card, Col, Row } from 'antd';
import { LineChartOutlined, FileTextOutlined, ClockCircleOutlined } from '@ant-design/icons';

const WarningCards = () => {
  const cardData = [
    {
      title: '监控预警设置',
      icon: <LineChartOutlined style={{ fontSize: '32px', color: '#ff9c6e' }} />,
      borderColor: '#ff9c6e',
    },
    {
      title: '预警阈值列表',
      icon: <FileTextOutlined style={{ fontSize: '32px', color: '#52c41a' }} />,
      borderColor: '#52c41a',
    },
    {
      title: '历史预警记录',
      icon: <ClockCircleOutlined style={{ fontSize: '32px', color: '#ff4d4f' }} />,
      borderColor: '#ff4d4f',
    },
  ];

  return (
    <Row gutter={[16, 16]} style={{ width: '200px', margin: 'auto' }}>
      {cardData.map((card, index) => (
        <Col span={24} key={index}>
          <Card
            bordered
            style={{
              borderLeft: `4px solid ${card.borderColor}`,
              textAlign: 'center',
            }}
            bodyStyle={{ padding: '24px' }}
          >
            <div style={{ fontSize: '32px', marginBottom: '8px' }}>{card.icon}</div>
            <div style={{ fontSize: '16px', fontWeight: 'bold' }}>{card.title}</div>
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default WarningCards;
