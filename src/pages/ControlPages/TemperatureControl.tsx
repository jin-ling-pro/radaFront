import React, { useState } from 'react';
import { Card, Button, Select, Input, Form, Row, Col } from 'antd';

const { Option } = Select;

const TemperatureControl: React.FC = () => {
  const [formData, setFormData] = useState({
    serialPort: 'COM6',
    baudRate: 115200,
    humidity: '',
    t1Temp: '',
    t2Temp: '',
    t3Temp: '',
    t4Temp: '',
    t5Temp: '',
    t6Temp: '',
  });

  return (
    <Card title="温湿度传感器" style={{ maxWidth: 600, margin: 'auto',border: '2px solid #d9d9d9', }}>
      <Form layout="vertical">
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="串口号">
              <Select defaultValue={formData.serialPort}>
                <Option value="COM6">COM6</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="波特率">
              <Input type="number" defaultValue={formData.baudRate} />
            </Form.Item>
          </Col>
        </Row>

        <Button type="primary" block>开串口</Button>

        <Form.Item label="倾角传感器温度">
          <Input value={formData.humidity} />
        </Form.Item>

        <Form.Item label="湿度传感器温度">
          <Input value={formData.humidity} />
        </Form.Item>

        <Row gutter={16}>
          {['t1Temp', 't2Temp', 't3Temp', 't4Temp', 't5Temp', 't6Temp'].map((temp, index) => (
            <Col span={12} key={temp}>
              <Form.Item label={`T${index + 1}温度`}>
                <Input value={formData[temp]} />
              </Form.Item>
            </Col>
          ))}
        </Row>

        <Button type="primary" block>读取</Button>
      </Form>
    </Card>
  );
};

export default TemperatureControl;
