import React from 'react';
import { Card, Button, Input, Row, Col, Form, Radio } from 'antd';

const RHIControl: React.FC = () => {
  return (
    <Card title="RHI 扫描设置" style={{ maxWidth: 600,border: '2px solid #d9d9d9', margin: 'auto' }}>
      <Form layout="vertical">
        <Form.Item label="方位角度">
          <Input defaultValue={90} type="number" />
        </Form.Item>

        <Form.Item label="俯仰细分数">
          <Input defaultValue={360} type="number" />
        </Form.Item>

        <Form.Item label="起始角度">
          <Input defaultValue={0} type="number" />
        </Form.Item>

        <Form.Item label="结束角度">
          <Input defaultValue={180} type="number" />
        </Form.Item>

        <Form.Item label="俯仰速度">
          <Input defaultValue={10} type="number" />
        </Form.Item>

        <Form.Item label="扫描模式">
          <Radio.Group defaultValue="持续扫">
            <Radio value="持续扫">持续扫</Radio>
            <Radio value="扇扫">扇扫</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item label="实时俯仰角">
          <Input defaultValue={92} type="number" />
        </Form.Item>


        <Form.Item label="圈数">
          <Input defaultValue={0} type="number" />
        </Form.Item>

        <Row gutter={16}>
          <Col span={12}>
            <Button type="primary" block>开始</Button>
          </Col>
          <Col span={12}>
            <Button danger block>结束</Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};

export default RHIControl;
