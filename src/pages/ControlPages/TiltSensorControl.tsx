import React from 'react';
import { Form, Input, Button, Select, Row, Col, Card } from 'antd';

const { Option } = Select;

const TiltSensorControl: React.FC = () => {
  return (
    <Card title="倾斜角传感器" style={{ maxWidth: 600, margin: 'auto',border: '2px solid #d9d9d9', }}>
      <Form layout="vertical">
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="串口号">
              <Select defaultValue="COM2">
                <Option value="COM1">COM1</Option>
                <Option value="COM2">COM2</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="波特率">
              <Input type="number" defaultValue="9600" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Button danger block>关串口</Button>
          </Col>
          <Col span={12}>
            <Button type="primary" block>读取</Button>
          </Col>
        </Row>

        <Form.Item label="X轴倾角">
          <Input type="number" defaultValue={0} />
        </Form.Item>

        <Form.Item label="Y轴倾角">
          <Input type="number" defaultValue={0} />
        </Form.Item>
      </Form>
    </Card>
  );
};

export default TiltSensorControl;
