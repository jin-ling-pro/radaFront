import React from 'react';
import { Card, Form, InputNumber, Select, Button, Row, Col } from 'antd';
import LaserControl from "@/pages/ControlPages/LaserControl";
import TiltSensorControl from "@/pages/ControlPages/TiltSensorControl";
import TemperatureControl from "@/pages/ControlPages/TemperatureControl";
import TurntableControl from "@/pages/ControlPages/TurntableControl";

const RadarControlPanel = () => {
  return (
    <div style={{display: 'flex', flexDirection: 'column', padding: '20px', backgroundColor: '#fff'}}>
      <div style={{display: 'flex', justifyContent: 'center', padding: '20px', backgroundColor: '#fff'}}>
        {/* 激光器控制面板 */}
        <div style={{width: '350px', margin: '10px'}}>
          <LaserControl/>
        </div>

        {/* 倾斜角传感器控制面板 */}
        <div style={{width: '350px', margin: '10px'}}>
          <TiltSensorControl/>
        </div>

        <div style={{width: '350px', margin: '10px'}}>
          <TemperatureControl/>
        </div>

        <div style={{width: '350px', margin: '10px'}}>
          <TurntableControl/>
        </div>
      </div>
    </div>
  );
}


export default RadarControlPanel;
