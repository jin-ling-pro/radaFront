import React from 'react';
import { Card, Button, Select, Input, Row, Col, Form } from 'antd';

const { Option } = Select;

const VADControl: React.FC = () => {
  return (
    <Card title="VAD/DBS 模式" style={{ maxWidth: 600, margin: 'auto',border: '2px solid #d9d9d9', }}>
      <Form layout="vertical">
        <Form.Item label="方位速度">
          <Input defaultValue={90} type="number" />
        </Form.Item>

        <Form.Item label="俯仰角度">
          <Input defaultValue={70} type="number" />
        </Form.Item>

        <Form.Item label="测试模式">
          <Select defaultValue="VAD">
            <Option value="VAD">VAD</Option>
            <Option value="DBS">DBS</Option>
          </Select>
        </Form.Item>

        <Form.Item label="圈数">
          <Input type="number" />
        </Form.Item>

        {/* 这个区域保留原来的方块 */}
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', marginBottom: '20px' }}>
          {['0°', '45°', '90°', '135°', '180°', '225°', '270°', '315°'].map(angle => (
            <div key={angle} style={{ width: '60px', height: '60px', backgroundColor: '#777', margin: '5px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              {angle}
            </div>
          ))}
        </div>

        <Row gutter={16}>
          <Col span={12}>
            <Button type="primary" block>开始</Button>
          </Col>
          <Col span={12}>
            <Button danger block>结束</Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};

export default VADControl;
