import React from 'react';
import { Form, Input, Button, Select, Row, Col, Card } from 'antd';

const { Option } = Select;

const TurntableControl: React.FC = () => {
  const [form] = Form.useForm();

  const handleFinish = (values: any) => {
    console.log('表单数据: ', values);
  };

  return (
    <Card title="转台控制" style={{ maxWidth: 600, margin: 'auto',border: '2px solid #d9d9d9', }}>
      <Form
        form={form}
        layout="vertical"
        onFinish={handleFinish}
        initialValues={{
          serialPort: 'COM4',
          baudRate: 115200,
        }}
      >
        <Row gutter={16}>
          {/* 串口号和波特率 */}
          <Col span={12}>
            <Form.Item label="串口号" name="serialPort">
              <Select>
                <Option value="COM1">COM1</Option>
                <Option value="COM2">COM2</Option>
                <Option value="COM3">COM3</Option>
                <Option value="COM4">COM4</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="波特率" name="baudRate">
              <Input type="number" />
            </Form.Item>
          </Col>
        </Row>

        {/* 开串口和关串口按钮 */}
        <Row gutter={16}>
          <Col span={12}>
            <Button type="primary" block>开串口</Button>
          </Col>
          <Col span={12}>
            <Button danger block>关串口</Button>
          </Col>
        </Row>

        {/* 写指令部分 */}
        <h3 style={{ marginTop: '20px' }}>写指令</h3>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="方位工作模式" name="azimuthModeWrite">
              <Select>
                <Option value="定位模式">定位模式</Option>
                <Option value="跟踪模式">跟踪模式</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="方位速度" name="azimuthSpeedWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="方位角度" name="azimuthAngleWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="俯仰工作模式" name="elevationModeWrite">
              <Select>
                <Option value="定位模式">定位模式</Option>
                <Option value="跟踪模式">跟踪模式</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="俯仰速度" name="elevationSpeedWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="俯仰角度" name="elevationAngleWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
        </Row>

        {/* 提交按钮 */}
        <Form.Item style={{ marginTop: '20px' }}>
          <Button type="primary" htmlType="submit" block>
            写指令
          </Button>
        </Form.Item>

        {/* 监视数据部分 */}
        <h3 style={{ marginTop: '20px' }}>监视数据</h3>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="方位工作模式" name="azimuthModeWrite">
              <Select>
                <Option value="定位模式">定位模式</Option>
                <Option value="跟踪模式">跟踪模式</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="方位速度" name="azimuthSpeedWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="方位角度" name="azimuthAngleWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="俯仰工作模式" name="elevationModeWrite">
              <Select>
                <Option value="定位模式">定位模式</Option>
                <Option value="跟踪模式">跟踪模式</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="俯仰速度" name="elevationSpeedWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="俯仰角度" name="elevationAngleWrite">
              <Input type="number" />
            </Form.Item>
          </Col>
        </Row>


      </Form>
    </Card>
  );
};

export default TurntableControl;
