import React from 'react';
import { Card, Button, Input, Row, Col, Form, Radio } from 'antd';

const PPIControl: React.FC = () => {
  return (
    <Card title="PPI 扫描设置" style={{ maxWidth: 600, margin: 'auto',border: '2px solid #d9d9d9', }}>
      <Form layout="vertical">
        <Form.Item label="俯仰角度">
          <Input defaultValue={70} type="number" />
        </Form.Item>
        <Form.Item label="方向细分数">
          <Input defaultValue={1} type="number" />
        </Form.Item>
        <Form.Item label="起始角度">
          <Input defaultValue={0} type="number" />
        </Form.Item>
        <Form.Item label="结束角度">
          <Input defaultValue={10} type="number" />
        </Form.Item>
        <Form.Item label="方向速度">
          <Input defaultValue={10} type="number" />
        </Form.Item>

        <Form.Item label="扫描模式">
          <Radio.Group defaultValue="持续扫">
            <Radio value="持续扫">持续扫</Radio>
            <Radio value="扇扫">扇扫</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item label="实时方向角">
          <Input defaultValue={10} type="number" />
        </Form.Item>



        <Form.Item label="圈数">
          <Input defaultValue={0} type="number" />
        </Form.Item>

        <Row gutter={16}>
          <Col span={12}>
            <Button type="primary" block>开始</Button>
          </Col>
          <Col span={12}>
            <Button danger block>结束</Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};

export default PPIControl;
