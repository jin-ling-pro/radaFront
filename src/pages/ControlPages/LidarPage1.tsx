import React from 'react';
import { Card, Form, InputNumber, Select, Button, Row, Col } from 'antd';
import PPIControl from "@/pages/ControlPages/PPIControl";
import RHIControl from "@/pages/ControlPages/RHIControl";
import VADControl from "@/pages/ControlPages/VADControl";
import LaserControl from "@/pages/ControlPages/LaserControl";
import TiltSensorControl from "@/pages/ControlPages/TiltSensorControl";
import TemperatureControl from "@/pages/ControlPages/TemperatureControl";
import TurntableControl from "@/pages/ControlPages/TurntableControl";

const RadarControlPanel = () => {
  return (
    <div style={{display: 'flex', flexDirection: 'column', padding: '20px', backgroundColor: '#fff'}}>


      <div style={{display: 'flex', justifyContent: 'center', padding: '20px', backgroundColor: '#fff'}}>
        {/* 激光器控制面板 */}
        <div style={{width: '350px', margin: '10px'}}>
          <VADControl/>
        </div>

        {/* 倾斜角传感器控制面板 */}
        <div style={{width: '350px', margin: '10px'}}>
          <PPIControl/>
        </div>

        <div style={{width: '350px', margin: '10px'}}>
          <RHIControl/>
        </div>
      </div>
    </div>
  );
}


export default RadarControlPanel;
