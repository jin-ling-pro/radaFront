import React, { useState } from 'react';
import {Card, Button, Select, Input, Row, Col, Form, message} from 'antd';
import axios from "axios";

const { Option } = Select;

const LaserControl: React.FC = () => {
  const [formData, setFormData] = useState({
    serialPort: 'COM1',
    baudRate: 115200,
    pulseWidth: '600ns',
    frequency: 10,
    power: 80,
    status: '关闭',
    outputPower: '',
    repetitionRate: '',
    systemCode: '',
    PA1Voltage: '',
    PA2Voltage: '',
    PA3Voltage: '',
    temperatures: {
      point1: '',
      point2: '',
      TEC1: '',
      TEC2: '',
      TEC3: '',
      TEC4: '',
      MCU: '',
    },
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  // 发送数据到后端
  const sendData = () => {
    axios.post('/api/radar/laserControl', formData)
      .then((response) => {
        message.success('数据发送成功');
      })
      .catch((error) => {
        message.error('数据发送失败: ' + error.message);
      });
  };

  return (
    <Card title="激光器控制" style={{ maxWidth: 800, margin: 'auto', border: '2px solid #d9d9d9' }}>
      <Form layout="vertical">
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="串口号">
              <Select value={formData.serialPort} onChange={(value) => handleChange({ target: { name: 'serialPort', value } })}>
                <Option value="COM1">COM1</Option>
                <Option value="COM2">COM2</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="波特率">
              <Input type="number" value={formData.baudRate} onChange={handleChange} name="baudRate" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="脉宽">
              <Select value={formData.pulseWidth} onChange={(value) => handleChange({ target: { name: 'pulseWidth', value } })}>
                <Option value="600ns">600ns</Option>
                <Option value="800ns">800ns</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="频率">
              <Input type="number" value={formData.frequency} onChange={handleChange} name="frequency" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="功率">
              <Input type="number" value={formData.power} onChange={handleChange} name="power" />
            </Form.Item>
          </Col>

        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="出光功率">
              <Input value={formData.outputPower} onChange={handleChange} name="outputPower" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="重复频率">
              <Input value={formData.repetitionRate} onChange={handleChange} name="repetitionRate" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="系统故障代码">
              <Input value={formData.systemCode} onChange={handleChange} name="systemCode" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="PA1电流">
              <Input value={formData.PA1Voltage} onChange={handleChange} name="PA1Voltage" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="PA2电流">
              <Input value={formData.PA2Voltage} onChange={handleChange} name="PA2Voltage" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="PA3电流">
              <Input value={formData.PA3Voltage} onChange={handleChange} name="PA3Voltage" />
            </Form.Item>
          </Col>
        </Row>

        {/* 温度部分 */}
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="点1温度">
              <Input value={formData.temperatures.point1} onChange={handleChange} name="point1" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="点2温度">
              <Input value={formData.temperatures.point2} onChange={handleChange} name="point2" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="TEC1温度">
              <Input value={formData.temperatures.TEC1} onChange={handleChange} name="TEC1" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="TEC2温度">
              <Input value={formData.temperatures.TEC2} onChange={handleChange} name="TEC2" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="TEC3温度">
              <Input value={formData.temperatures.TEC3} onChange={handleChange} name="TEC3" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="TEC4温度">
              <Input value={formData.temperatures.TEC4} onChange={handleChange} name="TEC4" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="MCU温度">
              <Input value={formData.temperatures.MCU} onChange={handleChange} name="MCU" />
            </Form.Item>
          </Col>
        </Row>

        {/* 读取按钮 */}
        <Row gutter={16}>
          <Col span={12}>
            <Button type="primary" block>读取</Button>
          </Col>
          <Col span={12}>
            <Button type="primary" block onClick={sendData}>发送</Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};

export default LaserControl;
