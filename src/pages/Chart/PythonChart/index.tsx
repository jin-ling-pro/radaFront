import React, { useState } from 'react';
import './indexStyle.css';
import PPIPythonPlot from "@/pages/Chart/PythonChart/PPI";
import RHIPythonPlot from "@/pages/Chart/PythonChart/RHI";
import DBSPlot from "./DBS";

const Index: React.FC = () => {
  const [selectedPlot, setSelectedPlot] = useState<string>('PPI');

  return (
    <div className="container" style={{display: 'flex', width: '100%', height: '100vh', alignItems: 'stretch'}}>
      {/* 左侧可切换区域 */}
      <div style={{
        width: '50%',
        height: '100%',
        border: '1px solid #ddd',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <div className="buttons" style={{display: 'flex', marginTop: '20px 0', justifyContent: 'center'}}>
          <button
            type="button"
            onClick={() => setSelectedPlot('PPI')}
            className={selectedPlot === 'PPI' ? 'active' : ''}
          >
            PPI
          </button>
          <button
            type="button"
            onClick={() => setSelectedPlot('RHI')}
            className={selectedPlot === 'RHI' ? 'active' : ''}
          >
            RHI
          </button>
          <button
            type="button"
            onClick={() => setSelectedPlot('VAD')}
            className={selectedPlot === 'VAD' ? 'active' : ''}
          >
            VAD
          </button>
          <button
            type="button"
            onClick={() => setSelectedPlot('DBS')}
            className={selectedPlot === 'DBS' ? 'active' : ''}
          >
            DBS
          </button>
        </div>

        <div style={{flexGrow: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', paddingLeft: '100px',}}>
          {selectedPlot === 'PPI' && <PPIPythonPlot/>}
          {selectedPlot === 'RHI' && <RHIPythonPlot/>}
        </div>
      </div>


      <div style={{
        width: '50%',
        height: '100%',
        border: '1px solid #ddd',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
      }}>
        <div className="buttons" style={{display: 'flex', margin: '20px 0', justifyContent: 'center'}}>
          <button
            type={'button'}
            onClick={() => setSelectedPlot('PPI')}
            className={selectedPlot === 'PPI' ? 'active' : ''}
          >
            DBS
          </button>
        </div>

        <div className="plot" style={{flexGrow: 1, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <DBSPlot/>
        </div>
      </div>
    </div>
  );
};

export default Index;
