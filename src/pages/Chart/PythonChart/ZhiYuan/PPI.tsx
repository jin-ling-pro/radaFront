import React from 'react';
import RadarConfigPage from '../../Layout/RadarConfigPage';

const PPIPage = () => (
  <RadarConfigPage
    title="PPI径向风速图"
    config={{
      speedLabel: '方位速度',
      speedPlaceholder: 'Azimuth Scan Speed 字符串范围: 0.0~360.0°',
      angleLabel: '俯仰角度',
      anglePlaceholder: 'Elevation 字符串范围: -2.0~90.0°',
      startAngleLabel: '起始方位角度',
      startAnglePlaceholder: 'Az Start 字符串范围: 0.0~360.0',
      endAngleLabel: '结束方位角度',
      endAnglePlaceholder: 'Az End 字符串范围: 0.0~360.0',
    }}
  />
);

export default PPIPage ;
