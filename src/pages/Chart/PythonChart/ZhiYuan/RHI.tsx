import React from 'react';
import RadarConfigPage from '../../Layout/RadarConfigPage';



const RHIPage = () => (
  <RadarConfigPage
    title="RHI径向风速图"
    config={{
      speedLabel: '俯仰速度',
      speedPlaceholder: 'Elevation Scan Speed 字符串范围: 0.0~90.0°',
      angleLabel: '方位角度',
      anglePlaceholder: 'Azimuth 字符串范围: 0.0~360.0°',
      startAngleLabel: '起始俯仰角度',
      startAnglePlaceholder: 'El Start 字符串范围: 0.0~360.0',
      endAngleLabel: '结束俯仰角度',
      endAnglePlaceholder: 'El End 字符串范围: 0.0~360.0',
    }}
  />
);

export default  RHIPage ;
