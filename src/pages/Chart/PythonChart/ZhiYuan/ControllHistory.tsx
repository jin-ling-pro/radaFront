import React from 'react';
import { ProTable, ProForm, ProFormText, ProFormSelect } from '@ant-design/pro-components';
import { Button, Tag } from 'antd';

const ExecutionRecordPage = () => {
  // 定义表格数据源，可以从接口获取实际数据
  const dataSource = [
    {
      key: 1,
      taskId: '98765422159',
      executor: '张三',
      status: '执行成功',
      mode: 'PPI',
      startTime: '2024-06-06 16:36:30',
      endTime: '2024-06-06 18:36:30',
      duration: '02小时00分01秒',
      reason: null,
    },
    {
      key: 2,
      taskId: '98765422159',
      executor: '李四',
      status: '执行失败',
      mode: 'PPI',
      startTime: '2024-06-06 16:36:30',
      endTime: '2024-06-06 18:36:30',
      duration: '02小时00分01秒',
      reason: '资源占用，执行异常',
    },
    // 更多数据
  ];

  // 定义表格列
  const columns = [
    {
      title: '序号',
      dataIndex: 'key',
    },
    {
      title: '任务ID',
      dataIndex: 'taskId',
    },
    {
      title: '执行人',
      dataIndex: 'executor',
    },
    {
      title: '执行状态',
      dataIndex: 'status',
      render: (text) => (
        text === '执行成功' ? <Tag color="green">执行成功</Tag> : <Tag color="red">执行失败</Tag>
      ),
    },
    {
      title: '工作模式',
      dataIndex: 'mode',
    },
    {
      title: '开始时间',
      dataIndex: 'startTime',
    },
    {
      title: '结束时间',
      dataIndex: 'endTime',
    },
    {
      title: '运行时长',
      dataIndex: 'duration',
    },
    {
      title: '失败原因',
      dataIndex: 'reason',
      render: (text) => text ? <Tag color="orange">{text}</Tag> : '无',
    },
    {
      title: '操作',
      dataIndex: 'operation',
      render: () => <Button type="link">查看</Button>,
    },
  ];

  return (
    <div>
      <ProForm layout="inline">
        <ProFormText name="executor" label="执行人" placeholder="请输入执行人" />
        <ProFormSelect name="status" label="执行状态" options={[{ label: '执行成功', value: '成功' }, { label: '执行失败', value: '失败' }]} />
        <ProFormSelect name="mode" label="工作模式" options={[{ label: 'PPI', value: 'PPI' }]} />
        {/*<Button type="primary">查询</Button>*/}
      </ProForm>
      <ProTable columns={columns} dataSource={dataSource} search={false} rowKey="key" pagination={{ pageSize: 5 }} />
    </div>
  );
};

export default ExecutionRecordPage;
