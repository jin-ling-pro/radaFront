import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import './style.css'; // 引入样式文件

interface Metadata {
  horizontal_wind_speed: number[];
  horizontal_wind_direction: number[];
  vertical_wind_speed: number[];
}

const DBSPlot: React.FC = () => {
  const [imgSrc, setImgSrc] = useState<string>('');
  // const [metadata, setMetadata] = useState<Metadata | null>(null);

  useEffect(() => {

    // 连接到 Flask-SocketIO 服务器
    const socket = io('http://localhost:5001', {
      transports: ['websocket', 'polling'],  // 使用WebSocket和轮询作为传输机制
    });

    socket.emit('get_dbs_data'); // 请求 DBS 图像

    socket.on('connect', () => {
      console.log('Connected to WebSocket server');
    });

    socket.on('disconnect', () => {
      console.log('Disconnected from WebSocket server');
    });

    socket.on('dbs_data', (data) => {
      console.log('Received radar data:', data);  // 打印接收到的整个数据
      if (data.image) {
        const base64Image = `data:image/png;base64,${data.image}`;
        setImgSrc(base64Image);
      } else {
        console.log('No image data received');
      }
    });

    // 清理 WebSocket 连接
    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <div className="container">

      {/* 显示图像 */}
      {imgSrc ? (
        <img
          src={imgSrc}
          alt="DBS"
          style={{
            maxWidth: '70%', // 确保图片不会超出容器宽度
            height: 'auto',
            marginTop: '10px',
            objectFit: 'contain'
          }}
        />
      ) : (
        <p style={{marginTop: '10px'}}>Loading...</p>
      )}

    </div>
  );
};

export default DBSPlot;
