import React, { useState } from 'react';
import VADPythonPlot from '../VAD'; // 引入VAD图像组件
import DBSPlot from '../DBS'; // 引入DBS图像组件
import {Button, Divider, Input, Radio, Form, Col, Row, message} from 'antd';
// import {response} from "express";


const VADDBSCombinedPage: React.FC = () => {
  const [mode, setMode] = useState('VAD'); // 使用状态变量来管理当前模式
  const [form] = Form.useForm(); // 创建 form 实例

  // 切换显示模式
  const handleModeChange = (newMode: string) => {
    setMode(newMode);
  };

  // 表单按钮
  // 表单提交逻辑，发送数据并开始雷达任务
  const handleFormSubmit = (values: any) => {

    const commandData: {
      detectionMode: null;
      elevationSpeed: null;
      startAngle: null;
      quantity: null;
      endAngle: null;
      azimuthAngle: null;
      azimuthSpeed: null;
      cycleNum: null;
      elevationMode: null;
      scanType: null;
      angleSync: null;
      remarks: null;
      elevationAngle: null
    } = {
      detectionMode: values.mode || null,
      azimuthSpeed: values.azimuthSpeed || null,
      azimuthAngle: values.azimuthAngle || null,
      elevationSpeed: values.elevationSpeed || null,
      elevationAngle: values.elevationAngle || null,
      startAngle: values.startAngle || null,
      endAngle: values.endAngle || null,
      scanType: values.scanType || null,
      quantity: values.quantity || null,
      angleSync: values.angleSync || null,
      remarks: values.remarks || null,
      elevationMode: values.elevationMode || null,
      cycleNum: values.cycleNum || null
    };


    const sanitizedValues = Object.fromEntries(
      Object.entries(values).map(([key, value]) => [key, value === undefined ? null : value])
    );

    // const signal =

    const requestData: API.VADRequest = {
      // ...sanitizedValues,
      commandType: mode, // 设置当前模式，例如 "VAD" 或 "DBS"
      commandData: commandData, // 嵌套的 commandData 对象
      signal: 'start'      // 开始信号
    };

    console.log("commandData: " + JSON.stringify(commandData, null, 2))
    console.log("values.azimuthSpeed: " + values.azimuthSpeed)
    console.log("mode: " + mode)

    // 发送合并后的请求
    fetch('/api/radar/startVAD', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(requestData)
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('发送表单数据失败');
        }
      })
      .then(data => {
        message.success('表单数据设置并启动成功');
        console.log('后端响应数据:', data);
      })
      .catch(error => {
        message.error(`操作失败: ${error.message}`);
        console.error('操作过程中出错:', error);
      });
  };

  // 串口状态
  const [isSerialPortOpen, setIsSerialPortOpen] = useState(false);
  // 切换串口状态
  const handleToggleSerialPort = () => {
    fetch('/api/radar/toggle', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        if (response.ok) {
          return response.json(); // 假设返回的数据是 JSON 格式
        } else {
          throw new Error('无法获取串口状态');
        }
      })
      .then(data => {
        setIsSerialPortOpen(data.isOpen); // 更新状态为后端返回的状态
        message.success(data.isOpen ? '串口已开启' : '串口已关闭');
      })
      .catch(error => {
        message.error('无法切换串口状态');
        console.error('请求串口状态时出错:', error.message);
      });
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '20px' }}>
      {/* 上方：切换模式按钮 */}
      <div style={{ display: 'flex', marginBottom: '10px' }}>
        <button
          style={{
            margin: '0 10px',
            padding: '8px 16px',
            backgroundColor: mode === 'VAD' ? '#007bff' : '#ddd',
            color: '#fff',
            border: 'none',
            cursor: 'pointer',
          }}
          onClick={() => handleModeChange('VAD')}
        >
          DBS水平风速、水平风向、垂直风速图
        </button>
        <button
          style={{
            margin: '0 10px',
            padding: '8px 16px',
            backgroundColor: mode === 'DBS' ? '#007bff' : '#ddd',
            color: '#fff',
            border: 'none',
            cursor: 'pointer',
          }}
          onClick={() => handleModeChange('DBS')}
        >
          DBS风廓线图
        </button>
      </div>

      {/* 中间：根据当前模式显示对应的图像 */}
      <div style={{ flex: 1, justifyContent: 'center', textAlign: 'center', width: '80%', height: '80%' }}>
        {mode === 'VAD' ? <VADPythonPlot /> : <DBSPlot />}
      </div>

      {/* 下方：配置表单 */}
      <div style={{
        flex: 1,
        padding: '20px',
        background: '#ffffff',
        borderRadius: '8px',
        boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)'
      }}>
        {/*<h2 style={{color: '#1a73e8'}}>{`${mode} 配置`}</h2>*/}
        <h2 style={{color: '#1a73e8'}}>{`${mode === 'VAD' ? 'DBS水平风速、水平风向、垂直风速图' : 'DBS风廓线图'}配置`}</h2>

        <Divider style={{margin: '12px 0', borderColor: '#d9d9d9'}}/>

        <Form layout="vertical" onFinish={handleFormSubmit} form={form}>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="方位速度" name="azimuthSpeed">
                <Input placeholder="Azimuth Scan Speed 范围: 0.0~360.0°"/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="俯仰角度" name="elevationAngle">
                <Input placeholder="Elevation 范围: 0.0~90.0°"/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="起始方位角度" name="startAngle">
                <Input placeholder="0.0" disabled/>
              </Form.Item>
              {/*  Az Start 字符串范围: 0.0~360.0  */}
            </Col>
            <Col span={12}>
              <Form.Item label="结束方位角度" name="endAngle">
                <Input placeholder="360.0" disabled/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="扫描方式" name="scanType">
                <Radio.Group>
                  <Radio value="持续扫">持续扫</Radio>
                  <Radio value="扇扫">扇扫</Radio>
                </Radio.Group>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="方位同步数" name="quantity">
                <Input placeholder="数据固定显示" disabled/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="实时方位角" name="angleSync">
                <Input placeholder="数据固定显示" disabled/>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="备注" name="remarks">
                <Input placeholder="数据固定显示" disabled/>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={16} justify="start">
            {/*<Col>*/}
            {/*  <Button type="default" style={{ marginRight: '8px', color: '#0073e6', borderColor: '#0073e6' }}>设置</Button>*/}
            {/*</Col>*/}
            <Col>
              <Button
                type="primary"
                htmlType="submit" // 设置表单的提交
                style={{backgroundColor: '#0073e6', borderColor: '#0073e6'}}
              >
                开始
              </Button>
            </Col>
            <Col>
              <Button type="default" style={{marginLeft: '8px', color: '#0073e6', borderColor: '#0073e6'}}>
                结束
              </Button>
            </Col>
            {/*<Col>*/}
            {/*  <Button type="default" onClick={handleToggleSerialPort}*/}
            {/*          style={{marginLeft: '8px', color: '#0073e6', borderColor: '#0073e6'}}>开启串口</Button>*/}
            {/*  <span style={{marginLeft: '10px', color: isSerialPortOpen ? 'green' : 'red'}}>*/}
            {/*    {isSerialPortOpen ? '串口已开启' : '串口未开启'}*/}
            {/*  </span>*/}
            {/*</Col>*/}
          </Row>
        </Form>

      </div>
    </div>
  );
};

export default VADDBSCombinedPage;
