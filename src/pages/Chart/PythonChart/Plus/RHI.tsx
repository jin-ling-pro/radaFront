import React from 'react';
import RHIPythonPlot from '../RHI';  // 导入用于显示图片的组件
import RadarConfigPage from '../../Layout/RadarConfigPage';  // 导入配置页面组件
import { Row, Col } from 'antd';  // 使用 Ant Design 布局

const RHICombinedPage = () => {
  return (
    <div style={{ padding: '20px' }}>
      <Row gutter={16}>
        {/* 左边图片展示 */}
        <Col span={12}>
          <div style={{ backgroundColor: '#f0f2f5', padding: '20px', borderRadius: '8px' }}>
            <h2>RHI径向风速图</h2>
            <RHIPythonPlot />
          </div>
        </Col>

        {/* 右边配置设置 */}
        <Col span={12}>
          <RadarConfigPage
            title="RHI设置"
            config={{
              speedLabel: '俯仰速度',
              speedPlaceholder: 'Elevation Scan Speed 字符串范围: 0.0~90.0°',
              angleLabel: '方位角度',
              anglePlaceholder: 'Azimuth 字符串范围: 0.0~360.0°',
              startAngleLabel: '起始俯仰角度',
              startAnglePlaceholder: 'El Start 字符串范围: 0.0~360.0',
              endAngleLabel: '结束俯仰角度',
              endAnglePlaceholder: 'El End 字符串范围: 0.0~360.0',
            }}
          />
        </Col>
      </Row>
    </div>
  );
};

export default RHICombinedPage;
