import React from 'react';
import PPIPythonPlot from '../PPI'; // 导入用于显示图片的组件
import { Row, Col, Input, Button } from 'antd';
import RadarConfigPage from "@/pages/Chart/Layout/RadarConfigPage"; // 使用 Ant Design 布局

const PPIPage = () => {
  return (
    <div style={{ padding: '20px' }}>
      <Row gutter={16}>
        {/* 左边图片展示 */}
        <Col span={12}>
          <div style={{ backgroundColor: '#f0f2f5', padding: '20px', borderRadius: '8px' }}>
            <h2>PPI径向风速图</h2>
            <PPIPythonPlot />
          </div>
        </Col>

        {/* 右边配置设置 */}
        <Col span={12}>
          <RadarConfigPage
            title="RHI设置"
            config={{
              speedLabel: '俯仰速度',
              speedPlaceholder: 'Elevation Scan Speed 字符串范围: 0.0~90.0°',
              angleLabel: '方位角度',
              anglePlaceholder: 'Azimuth 字符串范围: 0.0~360.0°',
              startAngleLabel: '起始俯仰角度',
              startAnglePlaceholder: 'El Start 字符串范围: 0.0~360.0',
              endAngleLabel: '结束俯仰角度',
              endAnglePlaceholder: 'El End 字符串范围: 0.0~360.0',
            }}
          />
        </Col>
      </Row>
    </div>
  );
};

export default PPIPage;
