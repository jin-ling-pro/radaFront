import React, { useState, useEffect } from 'react';
import { ProCard } from '@ant-design/pro-components';
import { Button } from 'antd';
import PPIPythonPlot from "@/pages/Chart/PythonChart/PPI";
import RHIPythonPlot from "@/pages/Chart/PythonChart/RHI";
import DBSPlot from "./DBS";
import './indexStyle.css';

const Index: React.FC = () => {
  const [selectedPlot, setSelectedPlot] = useState<string>('PPI');
  const [imageSrc, setImageSrc] = useState<string | null>(null);

  // 从后端获取图片
  useEffect(() => {
    const fetchImage = async () => {
      try {
        const response = await fetch('/api/get-image'); // 替换为实际后端接口
        const blob = await response.blob();
        const url = URL.createObjectURL(blob);
        setImageSrc(url);
      } catch (error) {
        console.error('图片加载失败:', error);
      }
    };
    fetchImage();
  }, []);

  return (
    <ProCard split="vertical" style={{ height: '100vh' }}>
      {/* 左侧按钮和图片展示区域 */}
      <ProCard colSpan="50%" bordered>
        <div className="buttons" style={{ display: 'flex', justifyContent: 'center', marginBottom: 20 }}>
          <Button type="primary" onClick={() => setSelectedPlot('PPI')} className={selectedPlot === 'PPI' ? 'active' : ''}>
            PPI
          </Button>
          <Button type="primary" onClick={() => setSelectedPlot('RHI')} className={selectedPlot === 'RHI' ? 'active' : ''}>
            RHI
          </Button>
          <Button type="primary" onClick={() => setSelectedPlot('VAD')} className={selectedPlot === 'VAD' ? 'active' : ''}>
            VAD
          </Button>
          <Button type="primary" onClick={() => setSelectedPlot('DBS')} className={selectedPlot === 'DBS' ? 'active' : ''}>
            DBS
          </Button>
        </div>
        <div className="plot" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%' }}>
          {selectedPlot === 'PPI' && <PPIPythonPlot />}
          {selectedPlot === 'RHI' && <RHIPythonPlot />}
          {selectedPlot === 'DBS' && <DBSPlot />}
        </div>
        {/*{imageSrc ? (*/}
        {/*  <img src={imageSrc} alt="从后端加载的图片" style={{ maxWidth: '100%', height: 'auto' }} />*/}
        {/*) : (*/}
        {/*  <div>图片加载中...</div>*/}
        {/*)}*/}
      </ProCard>

      {/* 右侧展示图表 */}
      <ProCard bordered>
        <div className="buttons" style={{display: 'flex', justifyContent: 'center', marginBottom: 20}}>
          <Button type="button">
            DBS
          </Button>
        </div>
        <div className="plot" style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%'}}>
          {<DBSPlot/>}
        </div>
      </ProCard>
    </ProCard>
  );
};

export default Index;
