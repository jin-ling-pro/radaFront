import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import './style.css'; // 引入样式文件

const PPIPythonPlot: React.FC = () => {
  const [imgSrc, setImgSrc] = useState<string>('');
  // const [metadata, setMetadata] = useState<Metadata | null>(null);

  useEffect(() => {

    // 连接到 Flask-SocketIO 服务器
    const socket = io('http://localhost:5000', {
      transports: ['websocket', 'polling'],  // 使用WebSocket和轮询作为传输机制
    });

    socket.emit('get_ppi_data'); // 请求 PPI 图像

    socket.on('connect', () => {
      console.log('Connected to WebSocket server');
    });

    socket.on('disconnect', () => {
      console.log('Disconnected from WebSocket server');
    });

    socket.on('ppi_data', (data) => {
      console.log('Received ppi_data:', data);  // 打印接收到的整个数据
      if (data.image) {
        const base64Image = `data:image/png;base64,${data.image}`;
        setImgSrc(base64Image);
      } else {
        console.log('No image data received');
      }
    });

    // 清理 WebSocket 连接
    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <div className="container">
      {/*<h1>PPI</h1>*/}

      {/* 显示图像 */}
      {imgSrc ? (
        <img
          src={imgSrc}
          alt="PPI"
          style={{
            maxWidth: '100%', // 确保图片不会超出容器宽度
            height: 'auto',
            // marginTop: '10px',
            objectFit: 'contain'
          }}
        />
      ) : (
        <p style={{marginTop: '10px'}}>Loading...</p>
      )}
    </div>
  );
};

export default PPIPythonPlot;


// import React, { useEffect, useState } from 'react';
//
// // 组件用于从Python获取并显示PPI图像
// const PPIPythonPlot: React.FC = () => {
//   const [imgSrc, setImgSrc] = useState<string>('');
//
//   useEffect(() => {
//     fetch('http://localhost:5001/')  // 假设PPI图像的API路径
//       .then(response => response.blob())
//       .then(blob => {
//         const url = URL.createObjectURL(blob);
//         setImgSrc(url);
//       })
//       .catch(error => console.error('Error fetching the PPI plot:', error));
//   }, []);
//
//   return (
//     <div>
//       <h1>PPI Wind Shear Detection Plot</h1>
//       {imgSrc ? (
//         <img src={imgSrc} alt="Python generated PPI plot" />
//       ) : (
//         <p>Loading PPI plot...</p>
//       )}
//     </div>
//   );
// };
//
// // 导出组件
// export default PPIPythonPlot;
