import React from 'react';
import { Heatmap } from '@ant-design/plots';

const generateData = () => {
  const data = [];
  const timeIntervals = 24; // 假设每小时一个时间点，共24个时间点
  const heightLevels = 10;  // 10个高度层次
  const maxDensity = 100;   // 假设最大密度值为100

  for (let t = 0; t < timeIntervals; t++) {
    for (let h = 0; h < heightLevels; h++) {
      data.push({
        time: `2024-09-01 ${String(10 + t).padStart(2, '0')}:00:00`,
        height: h, // 假设每层高度差为750米
        density: Math.floor(Math.random() * maxDensity) + 1, // 随机生成密度值，并确保不为0
      });
    }
  }

  return data;
};

const HeatmapChart = () => {
  const data = generateData();

  const config = {
    data,
    xField: 'time',
    yField: 'height',
    colorField: 'density',
    color: ['#ffffe0', '#00bfff', '#00008b'], // 颜色梯度
    axis: {
      x: {
        title: "Time",
        labelFill: '#000',
      },
      y: {
        title: "Height (m)",
        min: 0,
        max: 7500,
      }
    },
    tooltip: {
      showMarkers: false,
      fields: ['time', 'height', 'density'],
      formatter: datum => ({
        name: 'Density',
        value: `${datum.density}`,
      }),
    },
    style: {
      opacity: 0.85,
    },
    mark: 'cell',
  };

  return <Heatmap {...config} style={{ height: 500 }} />;
};

export default HeatmapChart;
