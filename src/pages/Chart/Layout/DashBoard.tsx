import { ProLayout, ProCard } from '@ant-design/pro-components';
import { useState } from 'react';
import LaserStatus from "@/pages/Status/Components/LaserStatus";
import LaserChart from "@/pages/Status/Components/LaserChart";

const DashBoard = () => {
  const [selectedMenu, setSelectedMenu] = useState('laser');

  const handleMenuClick = (key) => {
    setSelectedMenu(key);
  };

  const renderContent = () => {
    switch (selectedMenu) {
      case 'laser':
        return (
          <div>
            <ProCard title="激光器状态">
              <LaserStatus />
              <LaserChart />
            </ProCard>
          </div>
        );
      case 'environment':
        return <ProCard title="环境状态"> {/* 环境相关数据展示 */} </ProCard>;
      case 'tilt':
        return <ProCard title="倾斜角传感器状态"> {/* 倾斜角相关数据展示 */} </ProCard>;
      case 'temperature':
        return <ProCard title="温湿度传感器状态"> {/* 温湿度传感器状态相关数据展示 */} </ProCard>;
      default:
        return null;
    }
  };

  return (
    <ProLayout
      layout={"side"}
      menuHeaderRender={false}
      menuItemRender={(item, dom) => (
        <a onClick={() => handleMenuClick(item.key)}>{dom}</a> // 确保传递 item.key
      )}
      menuDataRender={() => [
        { key: 'laser', name: '激光器状态' },
        { key: 'environment', name: '环境状态' },
        { key: 'tilt', name: '倾斜角传感器状态' },
        { key: 'temperature', name: '温湿度传感器状态' },
      ]}
    >
      <div style={{ padding: 24 }}>{renderContent()}</div>
    </ProLayout>
  );
};

export default DashBoard;
