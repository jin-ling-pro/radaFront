import React from 'react';
import { ProForm, ProFormText, ProFormRadio, ProFormGroup, ProFormSwitch, ProFormSelect } from '@ant-design/pro-components';
import { Button, Divider } from 'antd';

const RadarConfigPage = ({ title, config }) => {
  return (
    <div style={{ padding: '20px', background: '#ffffff', borderRadius: '8px', boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)' }}>
      <h2 style={{ color: '#1a73e8' }}>{title}</h2>
      <Divider style={{ margin: '12px 0', borderColor: '#d9d9d9' }} />

      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '16px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>配置设置</h3>

        <ProForm layout="horizontal" submitter={false}>
          <ProFormGroup>
            {/* 激光器参数设置 */}
            {config.comPortLabel && (
              <ProFormText
                name="comPort"
                label={config.comPortLabel}
                placeholder={config.comPortPlaceholder}
              />
            )}
            {config.baudRateLabel && (
              <ProFormText
                name="baudRate"
                label={config.baudRateLabel}
                placeholder={config.baudRatePlaceholder}
              />
            )}
            {config.aomFrequencyShiftLabel && (
              <ProFormText
                name="aomFrequencyShift"
                label={config.aomFrequencyShiftLabel}
                placeholder={config.aomFrequencyShiftPlaceholder}
              />
            )}
            {config.laserWavelengthLabel && (
              <ProFormText
                name="laserWavelength"
                label={config.laserWavelengthLabel}
                placeholder={config.laserWavelengthPlaceholder}
              />
            )}
            {config.comPortSwitchLabel && (
              <ProFormSwitch
                name="comPortSwitch"
                label={config.comPortSwitchLabel}
                checkedChildren="开"
                unCheckedChildren="关"
              />
            )}
            {config.laserSwitchLabel && (
              <ProFormSwitch
                name="laserSwitch"
                label={config.laserSwitchLabel}
                checkedChildren="开"
                unCheckedChildren="关"
              />
            )}
          </ProFormGroup>

          <ProFormGroup>
            {/* 转台参数设置 */}
            {config.scanSpeedAzimuthLabel && (
              <ProFormText
                name="scanSpeedAzimuth"
                label={config.scanSpeedAzimuthLabel}
                placeholder={config.scanSpeedAzimuthPlaceholder}
              />
            )}
            {config.scanSpeedElevationLabel && (
              <ProFormText
                name="scanSpeedElevation"
                label={config.scanSpeedElevationLabel}
                placeholder={config.scanSpeedElevationPlaceholder}
              />
            )}
          </ProFormGroup>

          <ProFormGroup>
            {/* 采集卡参数设置 */}
            {config.samplesPerGateLabel && (
              <ProFormText
                name="samplesPerGate"
                label={config.samplesPerGateLabel}
                placeholder={config.samplesPerGatePlaceholder}
              />
            )}
            {config.pulsesNumberLabel && (
              <ProFormText
                name="pulsesNumber"
                label={config.pulsesNumberLabel}
                placeholder={config.pulsesNumberPlaceholder}
              />
            )}
          </ProFormGroup>

          <div style={{ textAlign: 'right', marginTop: '16px' }}>
            <Button type="primary" style={{ marginRight: '8px' }}>确定</Button>
            <Button>取消</Button>
          </div>
        </ProForm>
      </div>
    </div>
  );
};

export default RadarConfigPage;
