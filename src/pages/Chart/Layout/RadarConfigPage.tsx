import React from 'react';
import { ProForm, ProFormText, ProFormRadio, ProFormGroup } from '@ant-design/pro-components';
import { Button, Divider } from 'antd';

const RadarConfigPage = ({ title, config }) => {
  return (
    <div style={{ padding: '20px', background: '#ffffff', borderRadius: '8px', boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)' }}>
      <h2 style={{ color: '#1a73e8' }}>{title}</h2>
      <Divider style={{ margin: '12px 0', borderColor: '#d9d9d9' }} />

      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '16px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500' }}>雷达运动方式配置</h3>

        <ProForm
          layout="horizontal"
          submitter={{
            render: () => (
              <>
                <Button type="default" style={{ marginRight: '8px', color: '#0073e6', borderColor: '#0073e6' }}>设置</Button>
                <Button type="primary" style={{ backgroundColor: '#0073e6', borderColor: '#0073e6' }}>开始</Button>
                <Button type="default" style={{ marginLeft: '8px', color: '#0073e6', borderColor: '#0073e6' }}>复位</Button>
              </>
            ),
          }}
        >
          <ProFormGroup>
            <ProFormText
              name="speed"
              label={config.speedLabel}
              placeholder={config.speedPlaceholder}
            />
            <ProFormText
              name="angle"
              label={config.angleLabel}
              placeholder={config.anglePlaceholder}
            />
          </ProFormGroup>

          <ProFormGroup>
            <ProFormText
              name="startAngle"
              label={config.startAngleLabel}
              placeholder={config.startAnglePlaceholder}
            />
            <ProFormText
              name="endAngle"
              label={config.endAngleLabel}
              placeholder={config.endAnglePlaceholder}
            />
          </ProFormGroup>

          <ProFormGroup>
            <ProFormRadio.Group
              name="scanType"
              label="扫描方式"
              options={[
                { label: '持续扫', value: '持续扫' },
                { label: '扇扫', value: '扇扫' },
              ]}
            />
          </ProFormGroup>

          <ProFormGroup>
            <ProFormText name="quantity" label="方位同步数" placeholder="数据固定显示" disabled />
            <ProFormText name="angleSync" label="实时方位角" placeholder="数据固定显示" disabled />
          </ProFormGroup>

          <ProFormText name="remarks" label="备注" placeholder="数据固定显示" disabled />
        </ProForm>
      </div>
    </div>
  );
};

export default RadarConfigPage;
