import React from 'react';
import { Line } from '@ant-design/charts';

const commonConfig = {
  point: {
    size: 3,
    shape: 'circle',
    style: {
      fill: 'lime',
      stroke: 'lime',
    },
  },
  lineStyle: {
    stroke: 'lime',
    lineWidth: 1,
    opacity: 0.8,
  },
  tooltip: {
    showMarkers: false,
    domStyles: {
      'g2-tooltip': {
        backgroundColor: '#000',
        border: '1px solid #333',
        color: 'white',
      },
    },
  },
  scale: {
    x: {
      nice: true,
      // domain: [0, 4000]
    },
    y: {
      nice: true,
      // domain: [0, 10000]
    }
  },
  axis: {
    x: {
      //标题设置
      titleStroke: "white",
      titleFontSize: "32",
      titleStrokeOpacity: "1",
      titleFill: '#000',
      //网格设置
      grid: true,
      gridLineDash: [0, 0], // 设置网格线虚线样式
      gridStroke: '#000', // 设置网格线颜色
      gridStrokeOpacity: 1, //透明度
      // tickCount: 8,
      // tickInterval: 10,
      // nice: true,

      // label: true,
      labelFill: "white",
      // labelFormatter: (value) => `${value} km/h`,
    },
    y: {
      // nice: true,
      // line: true,
      // lineLineWidth: "3",
      // lineStrokeOpacity: "1",
      //标题字体设置
      titleStroke: "white",
      titleFontSize: "32",
      titleStrokeOpacity: "1",
      titleFill: '#000',

      label: true,
      labelFill: "white",

      grid: true,
      gridLineDash: [0, 0], // 设置网格线虚线样式
      gridStroke: '#000', // 设置网格线颜色
      gridStrokeOpacity: 1, //透明度
    },
  },
  color: 'lime',
};

function getRandomArbitrary(min: number, max: number): number {
  const i = Math.random() * (max - min) + min;
  if(i > 4000){
    return 4000;
  }else return i;
  // return Math.random() * (max - min) + min;
}

const HorizontalWindSpeedChart = () => {
  const data = [];
  for (let i = 0; i <= 100; i++) {
    data.push({ distance: i * 100, velocity: getRandomArbitrary(i, i * 50) });
  }

  const config = {
    ...commonConfig,
    data,
    axis: {
      ...commonConfig.axis,
      x: {
        ...commonConfig.axis.x,
        title: 'velocity',
        tickValues: [0, 1000, 2000, 3000, 4000, 4500],
      },
      y: {
        ...commonConfig.axis.y,
        title: 'distance',
      },
    },
    xField: 'velocity',
    yField: 'distance',
  };

  return <Line {...config} />;
};

export default HorizontalWindSpeedChart;
