import React, {useState} from 'react';
import { Line } from '@ant-design/charts';
import {Button} from "antd";
import {initUsingPost, startUsingPost} from "@/services/radar-backend/radarController";

const commonConfig = {
  // point: {
  //   size: 3,
  //   shape: 'circle',
  //   style: {
  //     fill: 'lime',
  //     stroke: 'lime',
  //   },
  // },
  lineStyle: {
    stroke: 'lime',
    lineWidth: 1,
    opacity: 0.8,
  },
  tooltip: {
    showMarkers: false,
    domStyles: {
      'g2-tooltip': {
        backgroundColor: '#000',
        border: '1px solid #333',
        color: 'white',
      },
    },
  },
  scale: {
    x: {
      nice: true,
      // domain: [0, 4000]
    },
    y: {
      nice: true,
      // domain: [0, 10000]
    }
  },
  axis: {
    x: {
      //标题设置
      titleStroke: "white",
      titleFontSize: "32",
      titleStrokeOpacity: "1",
      titleFill: '#000',
      //网格设置
      grid: true,
      gridLineDash: [0, 0], // 设置网格线虚线样式
      gridStroke: '#000', // 设置网格线颜色
      gridStrokeOpacity: 1, //透明度

      // label: true,
      labelFill: "#000",
      // labelFormatter: (value) => `${value} km/h`,
    },
    y: {
      // line: true,
      // lineLineWidth: "3",
      // lineStrokeOpacity: "1",
      //标题字体设置
      titleStroke: "white",
      titleFontSize: "32",
      titleStrokeOpacity: "1",
      titleFill: '#000',

      label: true,
      labelFill: "#000",

      grid: true,
      gridLineDash: [0, 0], // 设置网格线虚线样式
      gridStroke: '#000', // 设置网格线颜色
      gridStrokeOpacity: 1, //透明度
    },
  },
  color: 'lime',
};
function getRandomArbitrary(min: number, max: number): number {
  const i = Math.random() * (max - min) + min;
  if(i > 4000){
    return 4000;
  }else return i;
  // return Math.random() * (max - min) + min;
}
function getRandomArbitraryDirection(min: number, max: number): number {
  const i = Math.random() * (max - min) + min;
  if(i > 360){
    return 360;
  }else return i;
}

// 水平风速
const HorizontalWindSpeed = () => {
  const data = [];
  for (let i = 0; i <= 100; i++) {
    data.push({ distance: i * 100, velocity: getRandomArbitrary(i , i * 50) });
  }

  const config = {
    ...commonConfig,
    data,
    axis: {
      ...commonConfig.axis,
      x:{
        ...commonConfig.axis.x,
        title: '水平风速(m/s)',
        tickValues: [0, 6, 12, 18, 24, 30], // 确保包含 0 和最后一个刻度
      },
      y:{
        ...commonConfig.axis.y, // 保留 y 轴设置
        title: '高度(m)',
      },
    },
    xField: 'velocity',
    yField: 'distance',
  };

  return <Line {...config} />;
};

// 水平风向
const HorizontalWindDirection = () => {
  const data = [];
  for (let i = 0; i <= 100; i++) {
    data.push({ distance: i * 100, direction: getRandomArbitraryDirection(0 , i * 2) });
  }
  // data[0] = 0

  const config = {
    ...commonConfig,
    data,
    scale: {
      x: {
        // nice: true,
        type: 'linear',
        domain: [0, 360],
        tickCount: 5,
        tickMethod: () => [0, 90, 180, 270, 360],
      },
    },
    axis: {
      ...commonConfig.axis,
      x:{
        ...commonConfig.axis.x,
        title: '水平风向(° )',
      },
      y:{
        ...commonConfig.axis.y, // 保留 y 轴设置
        title: '高度(m)',
      },
    },

    xField: 'direction',
    yField: 'distance',
  };
  // direction
  return <Line {...config} />;
};

//垂直风速
const VerticalWindSpeed = () => {
  const data = [];
  for (let i = 0; i <= 100; i++) {
    data.push({ distance: i * 100, velocity: getRandomArbitrary(i * 10, i * 50)});
  }

  const config = {
    ...commonConfig,
    data,
    scale: {
      ...commonConfig.scale,
      x:{
        domain: [0, 4000]
      },
    },
    axis: {
      ...commonConfig.axis,
      x:{
        ...commonConfig.axis.x,
        title: '垂直风速(m/s)',
      },
      y:{
        ...commonConfig.axis.y, // 保留 y 轴设置
        title: '高度(m)',
      },
    },
    xField: 'velocity',
    yField: 'distance',
  };

  return <Line {...config} />;
};

const WindCharts = () => {
  //初始化和开始按钮
  const [isRadarStarted, setIsRadarStarted] = useState(false); // 状态管理雷达是否成功开启
  const [initSuccess, setInitSuccess] = useState(false); // 状态管理雷达是否成功初始化
  const handleButtonClickInit = async () => {
    try {
      const response = await initUsingPost({
        commandType: 'init',
        commandData: null,
      });

      console.log('Response from server:', response);
      setInitSuccess(response.data); // 根据服务器返回的响应更新状态
      // 处理服务器返回的数据
    } catch (error) {
      console.error('Error during init request:', error);
    }
  };

  const handleButtonClickStart = async () => {
    try {
      const response = await startUsingPost({
        commandType: 'start',
        commandData: null,
      });

      console.log('Response from server:', response);
      setIsRadarStarted(response.data); // 根据服务器返回的响应更新状态
      // 处理服务器返回的数据
    } catch (error) {
      console.error('Error during init request:', error);
    }
  };

  return (
    <div style={{display: 'flex', justifyContent: 'space-around', backgroundColor: '#fff', padding: '20px'}}>
      <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
        <Button type="default" style={{width: '100px', marginTop: '20px'}} onClick={handleButtonClickInit}>init</Button>
        <Button type="default" style={{width: '100px', marginTop: '20px'}}
                onClick={handleButtonClickStart}>start</Button>
        <div style={{marginTop: '20px', color: 'white'}}>
          <h3>雷达状态</h3>
          <p>初始化状态: {initSuccess ? "成功" : "未成功"}</p>
          <p>启动状态: {isRadarStarted ? "已启动" : "未启动"}</p>
        </div>
      </div>
      <div style={{flex: 1, padding: '0 10px'}}>
        <h3 style={{textAlign: 'center', color: 'white'}}>水平风速</h3>
        <HorizontalWindSpeed/>
      </div>
      <div style={{flex: 1, padding: '0 10px'}}>
        <h3 style={{textAlign: 'center', color: 'white'}}>水平风向</h3>
        <HorizontalWindDirection/>
      </div>
      <div style={{flex: 1, padding: '0 10px'}}>
        <h3 style={{textAlign: 'center', color: 'white'}}>垂直风速</h3>
        <VerticalWindSpeed/>
      </div>
    </div>

  );
};

export default WindCharts;
