import React, { useEffect, useState } from 'react';
import { Line } from '@ant-design/charts';
import { Button } from 'antd';
import { io } from 'socket.io-client';

const commonConfig = {
  // lineStyle: {
  //   stroke: 'lime',
  //   lineWidth: 1,
  //   opacity: 0.8,
  // },
  tooltip: {
    showMarkers: false,
    domStyles: {
      'g2-tooltip': {
        backgroundColor: '#000',
        border: '1px solid #333',
        color: 'white',
      },
    },
  },
  scale: {
    x: {
      // domain: [0, 20]
    },
    // y: { nice: true },
  },
  axis: {
    x: {
      grid: true,
      gridStroke: '#000',
      gridStrokeOpacity: 1
    },
    y: {
      grid: true, gridStroke: '#000', gridStrokeOpacity: 1
    },
  },
  color: 'lime',
};

// 水平风速图表
const HorizontalWindSpeed = ({ data }) => {
  const config = {
    ...commonConfig,
    data,
    xField: 'speed',
    yField: 'height',
    scale: {
      x: {
        domain: [0, 100]
      },
      y: {
        domain: [0, 100],
      },
    },
    axis: {
      x: {
        title: '水平风速(m/s)'
      },
      y: {
        title: '高度(m)'
      },
    },
  };
  return <Line {...config} />;
};

// 水平风向图表
const HorizontalWindDirection = ({ data }) => {
  const config = {
    ...commonConfig,
    data,
    xField: 'direction',
    yField: 'height',
    scale: {
      x: {
        type: 'linear', domain: [0, 360], tickCount: 5, tickMethod: () => [0, 90, 180, 270, 360]
      },
      y: {
        domain: [0, 100],
      },
    },
    axis: {
      x: {
        title: '水平风向(°)'
      },
      y: {
        title: '高度(m)'
      },
    },
  };
  return <Line {...config} />;
};

// 垂直风速图表
const VerticalWindSpeed = ({ data }) => {
  const config = {
    ...commonConfig,
    data,
    xField: 'speed',
    yField: 'height',
    scale: {
      x: {
        domain: [-50, 50]  // 可以根据数据的最大值适当调整，如 0-10 或 0-20
      },
    },
    axis: {
      x: {
        title: '垂直风速(m/s)'
      },
      y: {
        title: '高度(m)'
      },
    },
  };
  return <Line {...config} />;
};


const WindCharts = () => {
  const [windData, setWindData] = useState({ height: [], ivh: [], ibeta0: [], ivf: [] });
  // 弧度转换为角度
  const radiansToDegrees = (radians) => radians * (180 / Math.PI);

  useEffect(() => {
    const socket = io('http://localhost:5000', {
      transports: ['websocket', 'polling'],
    });

    socket.on('connect', () => {
      console.log('Connected to WebSocket server');
    });

    socket.on('radar_data', (data) => {
      console.log('Received wind data:', data);
      // console.log('ivf: ', data.data.ivf);
      setWindData(data.data);
    });

    socket.on('disconnect', () => {
      console.log('Disconnected from WebSocket server');
    });

    return () => {
      socket.disconnect();
    };
  }, []);

  // 将数据映射到图表数据格式
  const horizontalWindSpeedData = windData.height.map((height, index) => ({
    height,
    speed: windData.ivh[index], // 对应水平风速
  }));

  const horizontalWindDirectionData = windData.height.map((height, index) => ({
    height,
    direction: radiansToDegrees(windData.ibeta0[index]),
    // direction: windData.ibeta0[index],  // 对应水平风向角
  }));

  const verticalWindSpeedData = windData.height.map((height, index) => ({
    height,
    speed: windData.ivf[index],  // 对应垂直风速
  }));


  return (
    <div style={{ display: 'flex', justifyContent: 'space-around', backgroundColor: '#fff', padding: '20px' }}>
      <div style={{ flex: 1, padding: '0 10px' }}>
        <h3 style={{ textAlign: 'center', color: 'white' }}>水平风速</h3>
        <HorizontalWindSpeed data={horizontalWindSpeedData} />
      </div>
      <div style={{ flex: 1, padding: '0 10px' }}>
        <h3 style={{ textAlign: 'center', color: 'white' }}>水平风向</h3>
        <HorizontalWindDirection data={horizontalWindDirectionData} />
      </div>
      <div style={{ flex: 1, padding: '0 10px' }}>
        <h3 style={{ textAlign: 'center', color: 'white' }}>垂直风速</h3>
        <VerticalWindSpeed data={verticalWindSpeedData} />
      </div>
    </div>
  );
};

export default WindCharts;
