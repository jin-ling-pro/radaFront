import React from 'react';
import { Input, Button, Divider } from 'antd';

const { TextArea } = Input;

const CalibrationInfo = () => {
  return (
    <div style={{ padding: '20px', background: '#ffffff', borderRadius: '8px', boxShadow: '0 4px 12px rgba(0, 0, 0, 0.1)' }}>
      <h2 style={{ color: '#1a73e8', fontWeight: 'bold', marginBottom: '20px' }}>指北标定</h2>

      <div style={{ background: '#f5faff', padding: '16px', borderRadius: '4px', marginBottom: '20px', border: '1px solid #cce7ff' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500', marginBottom: '0' }}>指北标定信息</h3>
      </div>

      <div style={{ display: 'flex', gap: '16px', marginBottom: '24px' }}>
        <div style={{ flex: 1 }}>
          <label style={{ display: 'block', marginBottom: '8px', color: '#333', fontWeight: '500' }}>任务名称</label>
          <Input placeholder="请输入任务名称" style={{ borderRadius: '4px', padding: '8px' }} />
        </div>
        <div style={{ flex: 1 }}>
          <label style={{ display: 'block', marginBottom: '8px', color: '#333', fontWeight: '500' }}>标定角度</label>
          <Input placeholder="请输入标定角度" suffix="°" style={{ borderRadius: '4px', padding: '8px' }} />
        </div>
      </div>

      <Divider style={{ margin: '24px 0', borderColor: '#d9d9d9' }} />

      <div style={{ marginBottom: '24px' }}>
        <h3 style={{ color: '#0073e6', fontWeight: '500', marginBottom: '12px' }}>参数标定记录</h3>
        <TextArea rows={8} placeholder="请输入参数标定记录" style={{ borderRadius: '4px', padding: '8px' }} />
      </div>

      <div style={{ display: 'flex', justifyContent: 'flex-end', gap: '12px' }}>
        <Button type="primary" style={{ backgroundColor: '#0073e6', borderColor: '#0073e6' }}>确定</Button>
        <Button>取消</Button>
      </div>
    </div>
  );
};

export default CalibrationInfo;
