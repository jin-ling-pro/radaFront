import React from 'react';
import { Table, Input, Button, DatePicker, Space, Pagination } from 'antd';

const { Search } = Input;

const dataSource = [
  {
    key: '1',
    task: '2024-08月指北标定',
    angle: '0.23',
    status: '在用参数',
    person: '张三',
    date: '2024/10/10',
    record: '激光测风雷达的参数标定是确保其测量准确性的关键步骤...',
  },
  // 可以添加更多数据项
];

const columns = [
  {
    title: '序号',
    dataIndex: 'key',
    key: 'key',
  },
  {
    title: '标定任务',
    dataIndex: 'task',
    key: 'task',
  },
  {
    title: '标定后角度',
    dataIndex: 'angle',
    key: 'angle',
  },
  {
    title: '参数状态',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: '标定人员',
    dataIndex: 'person',
    key: 'person',
  },
  {
    title: '标定时间',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: '参数标定记录',
    dataIndex: 'record',
    key: 'record',
  },
  {
    title: '操作',
    key: 'action',
    render: () => (
      <Space size="middle">
        <a>详情</a>
        <a>删除</a>
      </Space>
    ),
  },
];

const CalibrationPage = () => {
  return (
    <div style={{ padding: '20px', background: '#f0f2f5', borderRadius: '8px' }}>
      <h2>指北标定</h2>

      <Space style={{ marginBottom: '16px' }}>
        <Search placeholder="输入标定任务" style={{ width: 200 }} />
        <DatePicker placeholder="选择标定时间" />
        <Button type="primary">查询</Button>
        <Button>重置</Button>
        <Button type="primary">标定</Button>
      </Space>

      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={{
          defaultCurrent: 1,
          total: 50,
          pageSize: 10,
          showQuickJumper: true,
        }}
        rowKey="key"
      />
    </div>
  );
};

export default CalibrationPage;
