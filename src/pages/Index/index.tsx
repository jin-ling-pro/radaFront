import React, { useState } from 'react';
import { Row, Col, Card, Button, Spin } from 'antd';
import TopInfo from './Components/TopInfo';
import EnvironmentStatus from './Components/EnvironmentStatus';
import WarningPanel from './Components/WarningPanel';
import PPIWindSpeed from './Components/PPIWindSpeed';
import RHIWindSpeed from './Components/RHIWindSpeed'; // 假设你有这个组件
import VADWindSpeed from './Components/VAD'; // 假设你有这个组件
import DBSWindSpeed from './Components/DBS'; // 假设你有这个组件
// import styles from './style.css';

const DashboardPage = () => {
  const [selectedChart, setSelectedChart] = useState('PPI');
  const [loading, setLoading] = useState(true);

  const handleChartChange = (chart) => {
    setLoading(true); // 设置为加载中
    setSelectedChart(chart);
    setTimeout(() => setLoading(false), 500); // 模拟加载时间
  };

  const renderChart = () => {
    if (loading) {
      return <Spin />;
    }
    switch (selectedChart) {
      case 'PPI':
        return <PPIWindSpeed />;
      case 'RHI':
        return <RHIWindSpeed />;
      case 'VAD':
        return <VADWindSpeed />;
      case 'DBS':
        return <DBSWindSpeed />;
      default:
        return <PPIWindSpeed />;
    }
  };

  return (
    <div style={{ padding: '16px', height: '100vh', width: '1700px', margin: '0 auto', backgroundColor: '#f0f2f5' }}>
      {/* 顶部信息栏 */}
      <TopInfo />

      <Row gutter={[12, 12]} style={{ marginTop: '16px' }}>
        {/* 左侧：设备环境状态和告警窗口 */}
        <Col span={8}>
          <Card style={{ marginBottom: '12px', height: '39vh' }}>
            <EnvironmentStatus />
          </Card>
          <Card style={{ height: '40vh' }}>
            <WarningPanel />
          </Card>
        </Col>

        {/* 右侧：风速图和切换按钮 */}
        <Col span={16}>
          <Card  style={{ height: '80vh' }}>
            <div style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
              {/* 按钮区域 */}
              <div style={{ textAlign: 'center', marginBottom: '16px' }}>
                <Button type={selectedChart === 'PPI' ? 'primary' : 'default'} onClick={() => handleChartChange('PPI')}>PPI</Button>
                <Button type={selectedChart === 'RHI' ? 'primary' : 'default'} onClick={() => handleChartChange('RHI')} style={{ marginLeft: '8px' }}>RHI</Button>
                <Button type={selectedChart === 'VAD' ? 'primary' : 'default'} onClick={() => handleChartChange('VAD')} style={{ marginLeft: '8px' }}>VAD</Button>
                <Button type={selectedChart === 'DBS' ? 'primary' : 'default'} onClick={() => handleChartChange('DBS')} style={{ marginLeft: '8px' }}>DBS</Button>
              </div>
              {/* 图表区域 */}
              <div style={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                {renderChart()}
              </div>
            </div>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default DashboardPage;
