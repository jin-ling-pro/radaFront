import React from 'react';
import { Card, Progress, Row, Col } from 'antd';

const StorageCapacity = () => (
  <Card title="内存容量" style={{ borderRadius: '8px', background: '#f5faff', border: '1px solid #cce7ff' }}>
    <Row>
      <Col span={12}>
        <h4>内存容量</h4>
        <Progress type="circle" percent={75} format={() => '1.5GB可用'} />
      </Col>
      <Col span={12}>
        <h4>外存容量</h4>
        <Progress type="circle" percent={50} format={() => '172GB可用'} />
      </Col>
    </Row>
  </Card>
);

export default StorageCapacity;
