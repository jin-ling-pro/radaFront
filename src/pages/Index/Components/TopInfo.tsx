import React from 'react';
import { Row, Col } from 'antd';

const TopInfo = () => (
  <Row justify="space-between" align="middle" style={{ padding: '16px', background: '#f0f2f5' }}>
    <Col>
      <span>{new Date().toLocaleString()}</span>
      <span style={{ marginLeft: '16px' }}>🌤️ 14°C 多云</span>
    </Col>
    <Col>
      <h2 style={{ fontSize: '24px', fontWeight: 'bold', color: '#0073e6' }}>激光测风雷达显控可视化大屏</h2>
    </Col>
    <Col>
      <button type={'button'} style={{ border: 'none', background: 'transparent', color: '#0073e6', cursor: 'pointer' }}>返回系统</button>
    </Col>
  </Row>
);

export default TopInfo;
