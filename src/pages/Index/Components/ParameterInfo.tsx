import React from 'react';

const ParameterInfo = () => {
  return (
    <div>
      <p>当前参数设置信息</p>
      <p>方位速度: 110°</p>
      <p>扫掠方式: 持续扫</p>
    </div>
  );
};

export default ParameterInfo;
