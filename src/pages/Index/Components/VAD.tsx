import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
// import './style.css'; // 引入样式文件

const VADPythonPlot: React.FC = () => {
  const [imgSrc, setImgSrc] = useState<string>('');

  useEffect(() => {
    // 连接到 Flask-SocketIO 服务器
    const socket = io('http://localhost:5001', {
      transports: ['websocket', 'polling'],  // 使用WebSocket和轮询作为传输机制
    });

    socket.emit('get_vad_data'); // 请求 VAD 图像

    socket.on('connect', () => {
      console.log('Connected to WebSocket server');
    });

    socket.on('disconnect', () => {
      console.log('Disconnected from WebSocket server');
    });

    socket.on('vad_data', (data) => {
      console.log('Received vad_data:', data);  // 打印接收到的整个数据
      if (data.image) {
        const base64Image = `data:image/png;base64,${data.image}`;
        setImgSrc(base64Image);
      } else {
        console.log('No image data received');
      }
    });

    // 清理 WebSocket 连接
    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <div className="container">

      {/* 显示图像 */}
      {imgSrc ? (
        <img
          src={imgSrc}
          alt="VAD Wind Speed"
          style={{
            // maxWidth: '100%',
            width: '80%',    // 设置图片宽度占满 Card
            height: '100%',
            objectFit: 'contain'
          }}
        />
      ) : (
        <p style={{ marginTop: '10px' }}>Loading...</p>
      )}
    </div>
  );
};

export default VADPythonPlot;
