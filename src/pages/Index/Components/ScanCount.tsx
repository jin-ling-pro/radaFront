import React from 'react';
import { Card } from 'antd';
import { Line } from '@ant-design/charts';

const ScanCount = () => {
  const data = [
    { date: '02-12', count: 2000 },
    { date: '03-12', count: 3000 },
    { date: '04-12', count: 2500 },
    { date: '05-12', count: 4000 },
    { date: '06-12', count: 3500 },
    { date: '07-12', count: 3800 },
    { date: '08-12', count: 3000 },
  ];

  const config = {
    data,
    xField: 'date',
    yField: 'count',
    // height: '500px',
    smooth: true,
    lineStyle: { stroke: '#0073e6', lineWidth: 2 },
    point: {
      size: 5,
      shape: 'circle',
      style: {
        fill: 'white',
        stroke: '#0073e6',
        lineWidth: 2,
      },
    },
    tooltip: {
      showMarkers: true,
    },
  };

  return (
    <Card title="扫描次数" style={{ borderRadius: '8px', background: '#f5faff', border: '1px solid #cce7ff' }}>
      <Line {...config} style={{ height: '90%', width: '100%' }} />
    </Card>
  );
};

export default ScanCount;
