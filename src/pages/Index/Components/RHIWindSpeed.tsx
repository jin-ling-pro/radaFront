import React, { useEffect, useState } from 'react';
import { io } from 'socket.io-client';

const RHIWindSpeed: React.FC = () => {
  const [imgSrc, setImgSrc] = useState<string>('');

  useEffect(() => {
    const socket = io('http://localhost:5000', {
      transports: ['websocket', 'polling'],
    });

    socket.emit('get_rhi_data');

    socket.on('connect', () => {
      console.log('Connected to WebSocket server');
    });

    socket.on('disconnect', () => {
      console.log('Disconnected from WebSocket server');
    });

    socket.on('rhi_data', (data) => {
      if (data.image) {
        const base64Image = `data:image/png;base64,${data.image}`;
        setImgSrc(base64Image);
      } else {
        console.log('No image data received');
      }
    });

    return () => {
      socket.disconnect();
    };
  }, []);

  return (
    <div className="container">
      {imgSrc ? (
        <img
          src={imgSrc}
          alt="RHI Wind Speed"
          style={{
            // maxWidth: '100%',
            width: '80%',    // 设置图片宽度占满 Card
            height: '100%',
            objectFit: 'contain'
          }}
        />
      ) : (
        <p style={{ marginTop: '10px' }}>Loading...</p>
      )}
    </div>
  );
};

export default RHIWindSpeed;
