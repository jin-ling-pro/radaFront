import React from 'react';
import { Table, Card } from 'antd';

const columns = [
  { title: '排名', dataIndex: 'rank', key: 'rank' },
  { title: '切变类型', dataIndex: 'type', key: 'type' },
  { title: '切变强度', dataIndex: 'intensity', key: 'intensity' },
  { title: '时间', dataIndex: 'time', key: 'time' },
];

const data = [
  { key: '1', rank: '1', type: '测风超限', intensity: '重度', time: '2024-01-12' },
  { key: '2', rank: '2', type: '测风超限', intensity: '重度', time: '2024-01-12' },
];

const WarningPanel = () => (
  <Card
    title="告警窗口"
    style={{
      borderRadius: '8px',
      background: '#f5faff',
      border: '1px solid #cce7ff',
      width: '100%', // 确保卡片宽度填充父容器
    }}
    bodyStyle={{ padding: '12px' }} // 调整内容区的内边距
  >
    <Table
      columns={columns}
      dataSource={data}
      pagination={false}
      bordered // 为表格添加边框
      style={{
        fontSize: '14px',
        textAlign: 'center', // 居中表格内容
      }}
    />
  </Card>
);

export default WarningPanel;
