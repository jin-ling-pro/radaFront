import React from 'react';
import { Card, Row, Col } from 'antd';

const EnvironmentStatus = () => (
  <Card title="设备环境状态" style={{ borderRadius: '8px', background: '#f5faff', border: '1px solid #cce7ff' }}>
    <Row gutter={[16, 16]}>
      <Col span={12}>室内温度: 31°C</Col>
      <Col span={12}>室内湿度: 24%</Col>
      <Col span={12}>室外温度: 18°C</Col>
      <Col span={12}>激光器温度: 24°C</Col>
    </Row>
  </Card>
);

export default EnvironmentStatus;
