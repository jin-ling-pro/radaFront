import React, { useState } from 'react';
import { Table, Button, Modal, Form, Input, message } from 'antd';

const UserManagement = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [users, setUsers] = useState([
    { key: 1, name: 'John Doe', email: 'john@example.com' },
    { key: 2, name: 'Jane Doe', email: 'jane@example.com' },
  ]);
  const [form] = Form.useForm();

  const handleAddUser = () => {
    setIsModalVisible(true);
  };

  const handleDeleteUser = (key: number) => {
    setUsers(users.filter(user => user.key !== key));
    message.success('用户删除成功');
  };

  const handleOk = () => {
    form
      .validateFields()
      .then((values) => {
        setUsers([...users, { key: users.length + 1, ...values }]);
        setIsModalVisible(false);
        form.resetFields();
        message.success('用户添加成功');
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const columns = [
    { title: '用户名', dataIndex: 'name', key: 'name' },
    { title: '邮箱', dataIndex: 'email', key: 'email' },
    {
      title: '操作',
      key: 'action',
      render: (text: any, record: any) => (
        <Button type="link" danger onClick={() => handleDeleteUser(record.key)}>
          删除
        </Button>
      ),
    },
  ];

  return (
    <>
      <Button type="primary" onClick={handleAddUser} style={{ marginBottom: 16 }}>
        添加用户
      </Button>
      <Table columns={columns} dataSource={users} />

      <Modal title="添加用户" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Form form={form} layout="vertical" name="userForm">
          <Form.Item
            name="name"
            label="用户名"
            rules={[{ required: true, message: '请输入用户名' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="email"
            label="邮箱"
            rules={[{ required: true, message: '请输入邮箱地址' }]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default UserManagement;
