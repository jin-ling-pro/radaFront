import React, { useRef } from 'react';
import { ProTable, ProForm, ModalForm, ProFormText, ProFormSelect } from '@ant-design/pro-components';
import { Button, Popconfirm, message } from 'antd';
import type { ProColumns, ActionType } from '@ant-design/pro-components';


// 示例数据
const data = [
  {
    id: 1,
    name: 'Admin',
    role: 'admin',
    status: 'active',
  },
  {
    id: 2,
    name: 'User',
    role: 'user',
    status: 'inactive',
  },
];

// 权限管理页面
const AccessManagement: React.FC = () => {
  const actionRef = useRef<ActionType>();

  // 定义表格列
  const columns: ProColumns[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      valueType: 'text',
    },
    {
      title: '用户名',
      dataIndex: 'name',
      valueType: 'text',
    },
    {
      title: '角色',
      dataIndex: 'role',
      valueType: 'select',
      valueEnum: {
        admin: { text: '管理员', status: 'Default' },
        user: { text: '用户', status: 'Default' },
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      valueType: 'select',
      valueEnum: {
        active: { text: '活跃', status: 'Success' },
        inactive: { text: '禁用', status: 'Error' },
      },
    },
    {
      title: '操作',
      valueType: 'option',
      render: (text, record) => [
        <ModalForm
          key="edit"
          title="编辑用户"
          trigger={<Button type="link">编辑</Button>}
          onFinish={async (values) => {
            message.success('编辑成功');
            actionRef.current?.reload();
            return true;
          }}
        >
          <ProFormText name="name" label="用户名" initialValue={record.name} />
          <ProFormSelect
            name="role"
            label="角色"
            initialValue={record.role}
            options={[
              { value: 'admin', label: '管理员' },
              { value: 'user', label: '用户' },
            ]}
          />
          <ProFormSelect
            name="status"
            label="状态"
            initialValue={record.status}
            options={[
              { value: 'active', label: '活跃' },
              { value: 'inactive', label: '禁用' },
            ]}
          />
        </ModalForm>,
        <Popconfirm
          key="delete"
          title="确定删除该用户吗？"
          onConfirm={() => {
            message.success('删除成功');
            actionRef.current?.reload();
          }}
        >
          <Button type="link" danger>
            删除
          </Button>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <ProTable
      headerTitle="权限管理"
      actionRef={actionRef}
      rowKey="id"
      columns={columns}
      dataSource={data}
      search={false}
      toolBarRender={() => [
        <ModalForm
          key="add"
          title="添加用户"
          trigger={<Button type="primary">添加用户</Button>}
          onFinish={async (values) => {
            message.success('添加成功');
            actionRef.current?.reload();
            return true;
          }}
        >
          <ProFormText name="name" label="用户名" placeholder="请输入用户名" rules={[{ required: true }]} />
          <ProFormSelect
            name="role"
            label="角色"
            options={[
              { value: 'admin', label: '管理员' },
              { value: 'user', label: '用户' },
            ]}
            rules={[{ required: true }]}
          />
          <ProFormSelect
            name="status"
            label="状态"
            options={[
              { value: 'active', label: '活跃' },
              { value: 'inactive', label: '禁用' },
            ]}
            rules={[{ required: true }]}
          />
        </ModalForm>,
      ]}
    />
  );
};

export default AccessManagement;
