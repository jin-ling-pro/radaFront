import React from 'react';
import { Form, Input, Select, Button, Row, Col } from 'antd';

const { Option } = Select;
const { TextArea } = Input;

const NewRecordPage = () => {
  const onFinish = (values) => {
    console.log('提交的表单数据: ', values);
  };

  return (
    <div style={{ padding: '24px', backgroundColor: '#f0f2f5' }}>
      <h2 style={{ color: '#1890ff', borderBottom: '2px solid #1890ff', marginBottom: '16px' }}>新增记录</h2>

      {/* 维护维修信息部分 */}
      <div style={{ backgroundColor: '#ffffff', padding: '24px', borderRadius: '8px', marginBottom: '16px' }}>
        <h3 style={{ color: '#1890ff', borderBottom: '1px solid #1890ff', paddingBottom: '8px' }}>维护维修信息</h3>
        <Form layout="vertical" onFinish={onFinish}>
          <Row gutter={16}>
            <Col span={8}>
              <Form.Item label="维护任务名称" name="taskName" rules={[{ required: true, message: '请输入任务名称' }]}>
                <Input placeholder="请输入内容" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="维护人员" name="maintainer" rules={[{ required: true, message: '请输入维护人员' }]}>
                <Input placeholder="请输入内容" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="联系方式" name="contact" rules={[{ required: true, message: '请输入联系方式' }]}>
                <Input placeholder="请输入内容" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={8}>
              <Form.Item label="维护类型" name="type" rules={[{ required: true, message: '请选择维护类型' }]}>
                <Select placeholder="请选择">
                  <Option value="常规运维">常规运维</Option>
                  <Option value="软件升级">软件升级</Option>
                  <Option value="设备更换">设备更换</Option>
                  <Option value="参数校准">参数校准</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="维护时间" name="time" rules={[{ required: true, message: '请输入维护时间' }]}>
                <Input placeholder="请输入内容" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>

      {/* 常规运维内容部分 */}
      <div style={{ backgroundColor: '#ffffff', padding: '24px', borderRadius: '8px' }}>
        <h3 style={{ color: '#1890ff', borderBottom: '1px solid #1890ff', paddingBottom: '8px' }}>常规运维</h3>
        <Form layout="vertical">
          <Form.Item label="运维内容" name="content">
            <TextArea
              rows={10}
              value={`激光测风雷达的常规运维内容主要包含以下几个方面：\n\n1. 日常巡检\n内容包括：检查设备是否正常运行、检查主备机切换状态、分析软件是否运行正常等。\n\n2. 周维护\n内容包括：清洁设备外部、检查各组件连接等。\n\n3. 月维护\n内容包括：更换易损件、检查设备状态等。\n\n...`}
            />
          </Form.Item>
        </Form>
      </div>

      {/* 操作按钮 */}
      <div style={{ textAlign: 'right', marginTop: '16px' }}>
        <Button type="primary" style={{ marginRight: '8px' }}>保存</Button>
        <Button>取消</Button>
      </div>
    </div>
  );
};

export default NewRecordPage;
