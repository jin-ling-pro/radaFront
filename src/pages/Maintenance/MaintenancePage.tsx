import React from 'react';
import { Table, Button, Input, Select, DatePicker, Form, Row, Col } from 'antd';

const { Option } = Select;

const MaintenancePage = () => {
  const columns = [
    { title: '序号', dataIndex: 'index', key: 'index' },
    { title: '任务名称', dataIndex: 'taskName', key: 'taskName' },
    { title: '维护类型', dataIndex: 'type', key: 'type' },
    { title: '维护人员', dataIndex: 'person', key: 'person' },
    { title: '联系方式', dataIndex: 'contact', key: 'contact' },
    { title: '维护时间', dataIndex: 'time', key: 'time' },
    {
      title: '操作',
      key: 'action',
      render: () => <Button type="link">查看</Button>,
    },
  ];

  const data = Array.from({ length: 10 }, (_, i) => ({
    key: i + 1,
    index: i + 1,
    taskName: '2024-08月常规运维-客户主动报修',
    type: ['常规运维', '软件升级', '设备更换', '参数校准'][i % 4],
    person: '张三',
    contact: '13100000000',
    time: '2024/10/10',
  }));

  const onSearch = (values) => {
    console.log('搜索条件:', values);
  };

  const onReset = () => {
    console.log('重置搜索条件');
  };

  return (
    <div style={{ padding: '24px', backgroundColor: '#ffffff', borderRadius: '8px' }}>
      <h2 style={{ color: '#1890ff', borderBottom: '2px solid #1890ff' }}>维护维修</h2>

      <Form layout="inline" onFinish={onSearch} style={{ marginBottom: '16px' }}>
        <Form.Item label="任务名称" name="taskName">
          <Input placeholder="请输入任务名称" />
        </Form.Item>
        <Form.Item label="维护类型" name="type">
          <Select placeholder="请选择维护类型" style={{ width: '120px' }}>
            <Option value="常规运维">常规运维</Option>
            <Option value="软件升级">软件升级</Option>
            <Option value="设备更换">设备更换</Option>
            <Option value="参数校准">参数校准</Option>
          </Select>
        </Form.Item>
        <Form.Item label="维护时间" name="time">
          <DatePicker placeholder="请选择日期" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">查询</Button>
        </Form.Item>
        <Form.Item>
          <Button onClick={onReset}>重置</Button>
        </Form.Item>
        <Form.Item>
          <Button type="dashed">新增</Button>
        </Form.Item>
      </Form>

      <Table
        columns={columns}
        dataSource={data}
        pagination={{
          pageSize: 10,
          showSizeChanger: true,
          showQuickJumper: true,
        }}
        bordered
        rowKey="key"
      />
    </div>
  );
};

export default MaintenancePage;
