import React from 'react';
import { Line } from '@ant-design/charts';

const TiltTemperatureData = [
  { time: '13:43', value: 10, type: '倾斜角传感器温度' },
  { time: '13:44', value: 20, type: '倾斜角传感器温度' },
  // 添加更多数据点...
];

const HumidityTemperatureData = [
  { time: '13:43', value: 15, type: '温湿度传感器湿度' },
  { time: '13:44', value: 25, type: '温湿度传感器湿度' },
  // 添加更多数据点...
];

const TTemperatureData = [
  { time: '13:43', value: 30, type: 'T1温度' },
  { time: '13:44', value: 35, type: 'T2温度' },
  // 添加更多数据点...
];


const LineChartComponent = ({ data, title, colors, xField = 'time', yField = 'value', seriesField }) => {
  const config = {
    data,
    xField,
    yField,
    seriesField,
    color: colors,
    lineStyle: {
      lineWidth: 2,
    },
    height: 200, // 可以根据需要调整高度
    legend: {
      position: 'top',
    },
    point: {
      size: 5,
      shape: 'circle',
    },
  };
  return (
    <div style={{ marginBottom: 20 }}>
      <h3>{title}</h3>
      <Line {...config} />
    </div>
  );
};
