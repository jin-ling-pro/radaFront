import React from 'react';
import { Line } from '@ant-design/charts';

const LineChartExample = () => {
  const generateData = (label) => {
    return Array.from({ length: 20 }, (_, index) => ({
      time: `13:${40 + index}`,
      value: Math.floor(Math.random() * 40),
      label: label,
    }));
  };

  // Example data for each chart
  const dataPA = [...generateData('PA1电流'), ...generateData('PA2电流'), ...generateData('PA3电流')];
  const dataPointTemp = [...generateData('点1温度'), ...generateData('点2温度'), ...generateData('点3温度')];
  const dataTECTemp = [...generateData('TEC1温度'), ...generateData('TEC2温度'), ...generateData('TEC3温度')];
  const dataMCUTemp = generateData('MCU温度');

  const config = (title, data) => ({
    data,
    xField: 'time',
    yField: 'value',
    seriesField: 'label',
    smooth: true,
    lineStyle: { lineWidth: 2 },
    yAxis: { label: { formatter: (v) => `${v}` } },
    xAxis: { label: { rotate: Math.PI / 6 } },
    tooltip: { showMarkers: false },
    title: { visible: true, text: title },
    height: 200,
  });

  return (
    <div>
      <div style={{ marginBottom: '24px' }}>
        <h3>PA电流</h3>
        <Line {...config('PA电流', dataPA)} />
      </div>
      <div style={{ marginBottom: '24px' }}>
        <h3>点温度</h3>
        <Line {...config('点温度', dataPointTemp)} />
      </div>
      <div style={{ marginBottom: '24px' }}>
        <h3>TEC温度</h3>
        <Line {...config('TEC温度', dataTECTemp)} />
      </div>
      <div style={{ marginBottom: '24px' }}>
        <h3>MCU温度</h3>
        <Line {...config('MCU温度', dataMCUTemp)} />
      </div>
    </div>
  );
};

export default LineChartExample;
