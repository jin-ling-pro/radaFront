import React from 'react';
import { Table, Input } from 'antd';

const RadarStatusGrid = () => {
  const dataSource = [
    {
      key: '1',
      status1: '串口号:',
      data1: '',
      status2: '波特率:',
      data2: '',
      status3: 'PN:',
      data3: '',
      status4: '序列号:',
      data4: '',
    },
    {
      key: '2',
      status1: '模块温度:',
      data1: '',
      status2: '系统电流:',
      data2: '',
      status3: '泵浦1电流:',
      data3: '',
      status4: '泵浦2电流:',
      data4: '',
    },
    {
      key: '3',
      status1: 'TEC1电流:',
      data1: '',
      status2: 'EDF3ATEC电流:',
      data2: '',
      status3: 'EDFA1电流:',
      data3: '',
      status4: 'EDFA2电流:',
      data4: '',
    },
    {
      key: '4',
      status1: 'UNL光程PD:',
      data1: '',
      status2: 'EDFA光程PD:',
      data2: '',
      status3: 'UNL_Mode:',
      data3: '',
      status4: 'AOM_status:',
      data4: '',
    },
    {
      key: '5',
      status1: 'AOM_Mode:',
      data1: '',
      status2: 'AOM频率:',
      data2: '',
      status3: 'AOM脉宽:',
      data3: '',
      status4: '外部同步信号频率:',
      data4: '',
    },
    {
      key: '6',
      status1: 'AOM延时时间1:',
      data1: '',
      status2: 'AOM延时时间2:',
      data2: '',
      status3: 'AOM延时时间3:',
      data3: '',
      status4: 'AOM延时时间4:',
      data4: '',
    },
    {
      key: '7',
      status1: '模块编号:',
      data1: '',
      status2: '系列状态:',
      data2: '',
      // status3: '',
      // data3: '',
      // status4: '',
      // data4: '',
    },
  ];

  const columns = [
    {
      title: '',
      dataIndex: 'status1',
      key: 'status1',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status2',
      key: 'status2',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status3',
      key: 'status3',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status4',
      key: 'status4',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
  ];

  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={false}
      bordered
      style={{ width: '100%' }}
    />
  );
};

export default RadarStatusGrid;
