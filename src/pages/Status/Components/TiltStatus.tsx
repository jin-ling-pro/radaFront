import React from 'react';
import { Table, Input } from 'antd';

const TiltStatus = () => {
  const dataSource = [
    {
      key: '1',
      status1: 'X轴倾角:',
      data1: '',
      status2: 'Y轴倾角:',
      data2: '',
    },
  ];

  const columns = [
    {
      title: '',
      dataIndex: 'status1',
      key: 'status1',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status2',
      key: 'status2',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
  ];

  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={false}
      bordered
      style={{ width: '100%' }}
    />
  );
};

export default TiltStatus;
