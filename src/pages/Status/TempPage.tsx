import React from 'react';
import { Table, Input } from 'antd';

const TempPage = () => {
  const dataSource = [
    {
      key: '1',
      status1: '倾角传感器温度:',
      data1: '',
      status2: '温湿度传感器温度:',
      data2: '',
      status3: '湿度:',
      data3: '',
      status4: '模块电池状态:',
      data4: '',
    },
    {
      key: '2',
      status1: 'T1温度:',
      data1: '',
      status2: 'T2温度:',
      data2: '',
      status3: 'T3温度:',
      data3: '',
      status4: 'T4温度:',
      data4: '',
    },
    {
      key: '3',
      status1: 'T5温度:',
      data1: '',
      status2: 'T6温度:',
      data2: '',
    },
  ];

  const columns = [
    {
      title: '',
      dataIndex: 'status1',
      key: 'status1',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status2',
      key: 'status2',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status3',
      key: 'status3',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status4',
      key: 'status4',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
  ];

  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={false}
      bordered
      style={{ width: '100%' }}
    />
  );
};

export default TempPage;
