import { ProLayout, ProCard } from '@ant-design/pro-components';
import React, { useState } from 'react';
import LaserStatus from "@/pages/Status/Components/LaserStatus";
import LaserChart from "@/pages/Status/Components/LaserChart";
import TiltStatus from "@/pages/Status/Components/TiltStatus";
import {Line} from "@ant-design/charts";

const LaserPage = () => {

  return (
    <>
      <div style={{width: '100%'}}>
        <div>
          <LaserStatus/>
        </div>
        <div style={{marginTop: '10px'}}>
          <LaserChart />
        </div>
      </div>
    </>
  );
};

export default LaserPage;
