import React from 'react';
import { Table, Input } from 'antd';

const RadarStatusGrid = () => {
  const dataSource = [
    {
      key: '1',
      status1: '经度:',
      data1: '',
      status2: '纬度:',
      data2: '',
      status3: '海拔:',
      data3: '',
      status4: 'EU壳内温度:',
      data4: '',
    },
    {
      key: '2',
      status1: 'EU壳内湿度:',
      data1: '',
      status2: 'EU壳内气压:',
      data2: '',
      status3: '箱内温度:',
      data3: '',
      status4: '箱内湿度:',
      data4: '',
    },
    {
      key: '3',
      status1: '箱内气压:',
      data1: '',
      status2: '外部温度:',
      data2: '',
      status3: '外部湿度:',
      data3: '',
      status4: '外部气压:',
      data4: '',
    },
  ];

  const columns = [
    {
      title: '',
      dataIndex: 'status1',
      key: 'status1',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status2',
      key: 'status2',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status3',
      key: 'status3',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
    {
      title: '',
      dataIndex: 'status4',
      key: 'status4',
      align: 'center',
      render: (text) => (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <span>{text}</span>
          <Input style={{ width: '80px', marginLeft: '8px' }} placeholder="" />
        </div>
      ),
    },
  ];

  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      pagination={false}
      bordered
      style={{ width: '100%' }}
    />
  );
};

export default RadarStatusGrid;
