import React from 'react';
import { Line } from '@ant-design/charts';
import TiltStatus from "@/pages/Status/Components/TiltStatus";
import {weight} from "@antv/g2/lib/data/utils/arc/sort";

const InclinationAngleChart = () => {
  const data = [
    { time: '13:43', value: 10, category: 'X轴倾斜角' },
    { time: '13:44', value: 15, category: 'X轴倾斜角' },
    { time: '13:45', value: 20, category: 'X轴倾斜角' },
    { time: '13:46', value: 25, category: 'X轴倾斜角' },
    { time: '13:47', value: 30, category: 'X轴倾斜角' },
    { time: '13:48', value: 35, category: 'X轴倾斜角' },
    { time: '13:49', value: 40, category: 'X轴倾斜角' },
    { time: '13:50', value: 45, category: 'X轴倾斜角' },
    { time: '13:51', value: 50, category: 'X轴倾斜角' },
    { time: '13:52', value: 55, category: 'X轴倾斜角' },
    { time: '13:43', value: 30, category: 'Y轴倾斜角' },
    { time: '13:44', value: 25, category: 'Y轴倾斜角' },
    { time: '13:45', value: 20, category: 'Y轴倾斜角' },
    { time: '13:46', value: 15, category: 'Y轴倾斜角' },
    { time: '13:47', value: 10, category: 'Y轴倾斜角' },
    { time: '13:48', value: 15, category: 'Y轴倾斜角' },
    { time: '13:49', value: 20, category: 'Y轴倾斜角' },
    { time: '13:50', value: 25, category: 'Y轴倾斜角' },
    { time: '13:51', value: 30, category: 'Y轴倾斜角' },
    { time: '13:52', value: 35, category: 'Y轴倾斜角' },
  ];

  const config = {
    data,
    autoFit: true,
    xField: 'time',
    yField: 'value',
    seriesField: 'category',
    color: ['#8DC63F', '#1C86EE'],
    legend: {
      position: 'bottom',
    },
    smooth: true,  // 平滑曲线
    yAxis: {
      title: {
        text: '倾斜角度',
      },
    },
    xAxis: {
      title: {
        text: '时间',
      },
      label: {
        rotate: Math.PI / 4,
        offset: 10,
      },
    },
  };

  return(
    <div style={{width: '70%'}}>
      <TiltStatus />
      <Line {...config} />
    </div>
    );
};

export default InclinationAngleChart;
