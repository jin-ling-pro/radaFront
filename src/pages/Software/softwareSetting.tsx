import React from 'react';
import { Table, Input, Button, DatePicker, Checkbox } from 'antd';

const { Search } = Input;

const SoftwareManagement = () => {
  const dataSource = [
    {
      key: '1',
      order: '1',
      name: '软件名称1',
      version: '1.1.2',
      maintenanceDate: '2024-09-01',
      status: '当前版本',
      updateDate: '2024-09-01',
      upgradeContent: '激光测风雷达的软件升级内容通常包括多个方面，旨在提升设备的性能、稳定性和功能。以下是一些常见的软...',
    },
    // 其他数据行...
  ];

  const columns = [
    {
      title: '选择',
      dataIndex: 'select',
      render: (_, record) => <Checkbox />,
    },
    {
      title: '序号',
      dataIndex: 'order',
    },
    {
      title: '软件名称',
      dataIndex: 'name',
    },
    {
      title: '软件版本号',
      dataIndex: 'version',
    },
    {
      title: '维护时间',
      dataIndex: 'maintenanceDate',
    },
    {
      title: '状态',
      dataIndex: 'status',
    },
    {
      title: '更新时间',
      dataIndex: 'updateDate',
    },
    {
      title: '软件升级内容',
      dataIndex: 'upgradeContent',
    },
    {
      title: '操作',
      dataIndex: 'operation',
      render: () => <a href="#details">详情</a>,
    },
  ];

  return (
    <div style={{ padding: '20px', background: '#f0f2f5', borderRadius: '8px' }}>
      <h2>软件管理</h2>

      <div style={{ display: 'flex', marginBottom: '16px', gap: '10px' }}>
        <Search placeholder="请输入软件名称" style={{ width: 200 }} />
        <DatePicker placeholder="请选择日期" />
        <Button type="primary">查询</Button>
        <Button>重置</Button>
        <Button type="primary">新增</Button>
      </div>

      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={{ pageSize: 5 }}
        rowSelection={{
          type: 'checkbox',
        }}
      />
    </div>
  );
};

export default SoftwareManagement;
