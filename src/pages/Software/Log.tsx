import React from 'react';
import { Input, Button, Form } from 'antd';

const { TextArea } = Input;

const SoftwareInfo = () => {
  return (
    <div style={{ padding: '20px', backgroundColor: '#f0f2f5', borderRadius: '8px' }}>
      <h2>新增软件信息</h2>

      <div style={{ padding: '10px', backgroundColor: '#ffffff', borderRadius: '8px', marginTop: '20px' }}>
        <div style={{ backgroundColor: '#e6f7ff', padding: '8px', borderRadius: '8px', fontWeight: 'bold' }}>
          软件基本信息
        </div>

        <Form layout="vertical" style={{ marginTop: '16px' }}>
          <Form.Item label="软件名称" name="softwareName">
            <Input placeholder="请输入内容" />
          </Form.Item>
          <Form.Item label="软件版本号" name="softwareVersion">
            <Input placeholder="请输入内容" />
          </Form.Item>
          <Form.Item label="软件升级内容" name="upgradeContent">
            <TextArea rows={10} placeholder="请输入内容" />
          </Form.Item>

          <div style={{ textAlign: 'right', marginTop: '20px' }}>
            <Button type="primary" style={{ marginRight: '10px' }}>保存</Button>
            <Button>取消</Button>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default SoftwareInfo;
